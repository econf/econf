<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Config;

class AddConferenceToUsersSettings extends Migration
{

    public function __construct()
    {
        if (version_compare(Application::VERSION, '5.0', '>=')) {
            $this->settingstablename = Config::get('settings.table');
        } else {
            $this->settingstablename = Config::get('anlutro/l4-settings::table');
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('conference_id')->unsigned()->nullable();
            $table->foreign('conference_id')->references('id')->on('conferences')->onDelete('cascade');
        });
        Schema::table($this->settingstablename, function (Blueprint $table) {
            $table->integer('conference_id')->unsigned()->nullable();
            $table->foreign('conference_id')->references('id')->on('conferences')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('conference_id');
        });

        Schema::table($this->settingstablename, function (Blueprint $table) {
            $table->dropColumn('conference_id');
        });
    }
}
