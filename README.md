# E-Conf

E-Conf is a Conference Management System designed to automate as much of the process as possible.

E-Conf is built on top of the [Laravel][link-laravel] framework.

This is the main app package.

![E-Conf](banner.png)

## Server requirements

To install E-Conf, your server must meet the following requirements:

* PHP >= 5.5.9
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension

Although not required, PHP >= 7.0.0 is recommended, as it improves E-Conf speed.

The server must also have installed:

* [Composer][link-composer]
* [Bower][link-bower]

## Installation

You can install E-Conf issuing the Composer `create-project` command in your terminal.
This command will create a fresh E-Conf installation in the directory you specify. For instance, the following
command will create a directory named `talks` containing a fresh E-Conf installation with all its dependencies
already installed.

``` bash
composer create-project econf/econf talks
```

## Configuration

#### Public directory

After installing E-Conf, you should configure the web server's document / web root to be the `public` directory.

#### Directory permissions

Directories within the `storage` and the `bootstrap/cache` directory should be writable by yout web server or
E-Conf will not run.

#### App URL

The URL where the app will be accessible must be defined on the `APP_URL` key in the `.env` file.

#### Multi-conference

Multi-conference may be turned on or off through the `ECONF_MULTI` key in the `.env` file, whose value must be
`true` or `false`.

You must **not** change this value after the application installation.

#### Database settings

Database settings must be defined according to the respective [Laravel documentation][laravel-docs-db].

After the database is configured, you need to prepare it. That can be done by running the following command in
the folder the app is installed:

``` bash
php artisan migrate
```

#### HTTPS

By default, E-Conf forces a HTTPS connection on production environments. This can be changed through the
`ECONF_SECURE` key in the `.env` file.

#### Installer

To conclude E-Conf configuration, open its URL on your web browser. You will be presented with an installer to
help you create your administration account.

## Related packages

E-Conf has additional packages that extend its functionality and are not included by default.
You can install them, if you want, by following the instructions in their repositories.

* [E-Conf Submissions][link-submissions-package]
* [E-Conf Reviews][link-reviews-package]
* [E-Conf Program Management][link-program-package]

## Credits

- [João Luís][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[link-author]: http://joaopluis.pt
[link-laravel]: http://laravel.com
[link-submissions-package]: https://gitlab.com/econf/submissions
[link-reviews-package]: https://gitlab.com/econf/reviews
[link-program-package]: https://gitlab.com/econf/program-management
[link-composer]:  https://getcomposer.org
[link-bower]: https://bower.io

[laravel-docs-db]: https://laravel.com/docs/5.2/database#configuration
