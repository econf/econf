<?php
return [

	/*
    |--------------------------------------------------------------------------
    | Multi-conference (multi-tenancy)
    |--------------------------------------------------------------------------
    |
    | If true, the system will act as a multi-conference system.
    |
    */

	'multi' => env('ECONF_MULTI', false),

	/*
    |--------------------------------------------------------------------------
    | Enforce HTTPS
    |--------------------------------------------------------------------------
    |
    | If true, the system will enforce a HTTPS connection except if the app is in debug mode.
    |
    */

	'secure' => env('ECONF_SECURE', true),

];