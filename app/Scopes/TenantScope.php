<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;

class TenantScope implements Scope {

	private $model;

	public static $tenant_col = 'conference_id';

	public function apply(Builder $builder, Model $model)
	{
		$builder->whereRaw($model->getTenantWhereClause(self::$tenant_col, self::getTenantId()));
	}

	public function remove(Builder $builder, Model $model)
	{
		$query = $builder->getQuery();
		foreach( (array) $query->wheres as $key => $where) {
			if($this->isTenantConstraint($model, $where, self::$tenant_col, self::getTenantId())) {
				unset($query->wheres[$key]);

				$query->wheres = array_values($query->wheres);
				break;
			}
		}
	}

	public function isTenantConstraint($model, array $where, $tenantColumn, $tenantId)
	{
		return $where['type'] == 'raw' && $where['sql'] == $model->getTenantWhereClause($tenantColumn, $tenantId);
	}

	public static function getTenantId(){
		if ( config( 'econf.multi' ) && !empty( config( 'econf.conference.id' ) ) ) {
			return config( 'econf.conference.id' );
		} else {
			return null;
		}
	}

}