<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 18/03/16
 * Time: 20:48
 */

namespace App;

use App;

class Country {

	protected $package_url = 'vendor/umpirsky/country-list/country/cldr';

	protected $filename = 'country.php';

	protected $country_list = null;

	private function getCountryList( $escape = false ) {
		if ( is_null( $this->country_list ) ) {
			$this->country_list = [ ];

			$list_url = base_path( $this->package_url . '/' . App::getLocale() . '/' . $this->filename );
			$this->country_list = @include( $list_url );
		}

		$cl = $this->country_list;

		if ( $escape ) {
			$cl = array_map( 'addslashes', $cl );
		}

		return $cl;
	}

	public function all() {
		return $this->getCountryList();
	}

	public function get( $code ) {
		return $this->getCountryList()[$code];
	}

	public function field( $label, $name, $placeholder = null, $value = '', $escape = false ) {

		$el = \BootForm::select( $label, $name, $this->getCountryList( $escape ) )
		               ->attribute( 'data-placeholder', $placeholder ?: $label )
		               ->attribute( 'data-allow-clear', 'true' )
		               ->addOption( '', '' )
		               ->addClass( 'select2' )
		               ->style( 'width:100%;' )
		               ->select( $value );

		if ( !empty( $value ) ) {
			$el->select( $value );
		}

		return $el;
	}

	public function validate( $attribute, $value, $parameters, $validator ) {
		return array_key_exists( $value, $this->getCountryList() );
	}
}