<?php

namespace App;

use App\Traits\TenantableTrait;
use Illuminate\Database\Eloquent\Model;

class SessionType extends Model
{

	use TenantableTrait;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'duration', 'parallel'];

}
