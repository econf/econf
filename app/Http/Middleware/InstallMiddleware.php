<?php

namespace App\Http\Middleware;

use Closure;
use Setting;

class InstallMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    	$installed = setting('installed', null, false);

	    if(!$installed){
		    $first = explode('/', $request->path())[0];
			if($first != 'install'){
				return redirect('install');
			}
	    }

        return $next($request);
    }
}
