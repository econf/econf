<?php

namespace App\Http\Middleware;

use App\Committee;
use Closure;

class ChairMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param array $committees
     * @return mixed
     * @internal param array|string ...$roles
     */
    public function handle($request, Closure $next, ...$committees)
    {
        if (! Committee::in($committees, true)) {
            return redirect(m_action('PublicController@index'));
        }

        return $next($request);
    }

}
