<?php

namespace App\Http\Middleware;

use App\Conference;
use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                $url = "";
                if(config('econf.multi')){
                    $url = config('econf.conference.slug')."/";
                }
                return redirect()->guest($url.'login');
            }
        }

        return $next($request);
    }
}
