<?php

namespace App\Http\Middleware;

use App\Committee;
use Closure;

class CommitteeMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param array $committees
     * @return mixed
     * @internal param array|string ...$roles
     */
    public function handle($request, Closure $next, ...$committees)
    {

        if(empty($committees)){
            $allowed = Committee::any();
        } else {
            $allowed = Committee::in($committees);
        }

        if (! $allowed) {
            return redirect(m_action('PublicController@index'));
        }

        return $next($request);
    }

}
