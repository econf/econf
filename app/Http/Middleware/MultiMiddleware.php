<?php

namespace App\Http\Middleware;

use App\Conference;
use App\Scopes\TenantScope;
use Closure;

use Setting;
use Mail;

class MultiMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {

		$conference = Conference::where( 'slug', $request->conf_slug )->firstOrFail();

		config( [ 'econf.conference.id' => $conference->id, 'econf.conference.slug' => $conference->slug ] );

		Setting::setExtraColumns(array(
			TenantScope::$tenant_col => $conference->id
		));

		$request->route()->forgetParameter('conf_slug');

		Mail::alwaysFrom(Setting::get('conf-email', config('mail.from.address')), Setting::get('conf-short_name', Setting::get('conf-name')));

		return $next( $request );
	}
}
