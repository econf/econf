<?php

namespace App\Http\Middleware;

use App;
use Auth;
use Closure;
use Cookie;
use Date;
use Session;
use Setting;

class LocaleMiddleware {
	/**
	 * @param $user
	 * @param $locale
	 */
	private static function storeUserLocale( $user, $locale ) {
		$data = $user->data;
		$data['locale'] = $locale;
		$user->data = $data;
		$user->save();
	}

	public static function setUserLocale( $user = null ) {

		if ( is_null( $user ) ) {
			$user = Auth::user();
		}

		$locale = Setting::get( 'locale', config( 'app.locale' ) );

		if ( !empty( $user ) && !empty( $user->data( 'locale' ) ) ) {
			// If user has locale, apply it

			$locale = $user->data( 'locale' );
		} else if ( Session::has( 'locale' ) ) {
			// If cookie exists, apply it. If user, define it.
			$locale = Session::get( 'locale' );
			if ( !empty( $user ) ) {
				self::storeUserLocale( $user, $locale );
			}
		} else {
			// Else, read from header and save cookie.
			$header = \Request::server( 'HTTP_ACCEPT_LANGUAGE' );
			$languages = collect( explode( ',', $header ) );
			$languages->transform( function ( $item, $key ) {
				$exploded = explode( ';', $item );
				return $exploded[0];
			} );
			$available_languages = array_keys( config( 'languages' ) );
			foreach ( $languages as $language ) {
				if ( in_array( $language, $available_languages ) ) {
					$locale = $language;
					if ( !empty( $user ) ) {
						self::storeUserLocale( $user, $locale );
					} else {
						Session::set( 'locale', $locale );
					}
					break;
				}
			}
		}

		self::setLocale( $locale );
	}

	/**
	 * @param $locale
	 */
	public static function setLocale( $locale ) {
		if ( in_array( $locale, array_keys( config( 'languages' ) ) ) ) {
			App::setLocale( $locale );
			Date::setLocale( $locale );
		}
	}


	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {

		self::setUserLocale();

		return $next( $request );
	}
}
