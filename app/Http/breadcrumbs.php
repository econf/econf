<?php

// Dashboard
Breadcrumbs::register('network.dashboard', function($breadcrumbs)
{
	$breadcrumbs->push(trans('econf.dashboard.dashboard'), action('Network\DashboardController@view'));
});

// Conferences
Breadcrumbs::register('network.conferences', function($breadcrumbs)
{
	$breadcrumbs->push(trans('econf.network.conferences'), action('Network\ConferenceController@index'));
});
// Conferences > New
Breadcrumbs::register('network.conferences.new', function($breadcrumbs)
{
	$breadcrumbs->parent('network.conferences');
	$breadcrumbs->push(trans('econf.network.new_conference'));
});
// Conferences > Single
Breadcrumbs::register('network.conferences.show', function($breadcrumbs, $conference)
{
	$breadcrumbs->parent('network.conferences');
	$breadcrumbs->push(setting('conf-short_name', $conference->id, setting('conf-name', $conference->id)));
});
// Conferences > Single > Edit
Breadcrumbs::register('network.conferences.edit', function($breadcrumbs, $conference)
{
	$breadcrumbs->parent('network.conferences.show', $conference);
	$breadcrumbs->push(trans('econf.actions.edit'));
});
// Account
Breadcrumbs::register('network.account', function($breadcrumbs)
{
	$breadcrumbs->push(trans('econf.user.account'));
});
// Account > Security
Breadcrumbs::register('network.account.security', function($breadcrumbs)
{
	$breadcrumbs->parent('network.account');
	$breadcrumbs->push(trans('econf.user.security'));
});
// Account > Profile
Breadcrumbs::register('network.account.profile', function($breadcrumbs)
{
	$breadcrumbs->parent('network.account');
	$breadcrumbs->push(trans('econf.user.profile'));
});

// Users
Breadcrumbs::register('network.users', function($breadcrumbs)
{
	$breadcrumbs->push(trans('econf.network.users'), action('Network\UserController@index'));
});
// Users > New
Breadcrumbs::register('network.users.new', function($breadcrumbs)
{
	$breadcrumbs->parent('network.users');
	$breadcrumbs->push(trans('econf.network.new_user'));
});
// Users > Edit
Breadcrumbs::register('network.users.edit', function($breadcrumbs)
{
	$breadcrumbs->parent('network.users');
	$breadcrumbs->push(trans('econf.network.edit_user'));
});

/**
 * ADMIN
 */

// Dashboard
Breadcrumbs::register('admin.dashboard', function($breadcrumbs)
{
	$breadcrumbs->push(trans('econf.dashboard.dashboard'), m_action('Admin\DashboardController@view'));
});

Breadcrumbs::register('admin.conference', function($breadcrumbs)
{
	$breadcrumbs->push(trans('econf.admin.conference'));
});

// Organization
Breadcrumbs::register('admin.organization', function($breadcrumbs)
{
	$breadcrumbs->push(trans('econf.admin.organization'));
});

// Org > Org committee
Breadcrumbs::register('committee', function($breadcrumbs, $committee)
{
	$breadcrumbs->parent('admin.organization');
	$breadcrumbs->push($committee->full_name());
});

// Settings
Breadcrumbs::register('admin.email', function($breadcrumbs)
{
	$breadcrumbs->parent('admin.organization');
	$breadcrumbs->push(trans('econf.admin.email.label'));
});

// Settings
Breadcrumbs::register('admin.settings', function($breadcrumbs)
{
	$breadcrumbs->parent('admin.organization');
	$breadcrumbs->push(trans('econf.admin.settings.label'));
});
// Settings > Conference
Breadcrumbs::register('admin.settings.conference', function($breadcrumbs)
{
	$breadcrumbs->parent('admin.settings');
	$breadcrumbs->push(trans('econf.admin.settings.conference.label_short'));
});
// Settings > Session types
Breadcrumbs::register('admin.settings.session_types', function($breadcrumbs)
{
	$breadcrumbs->parent('admin.settings');
	$breadcrumbs->push(trans('econf.session_types.label'));
});