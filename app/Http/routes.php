<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

/*
 * Routes needed for a single conference.
 */
use App\Scopes\TenantScope;

function conference_routes() {

	Route::group( [ 'middleware' => 'locale' ], function () {

		Route::get( '/', 'PublicController@index' );
		Route::get( 'manifest.json', 'ManifestController@get' );
		Route::auth();
		Route::get( 'twofactor', 'Auth\AuthController@showTwoFactorForm' );
		Route::post( 'twofactor', 'Auth\AuthController@twoFactor' );

		Route::group( [ 'prefix' => 'invite/{token}' ], function () {
			Route::get( '/', 'InviteController@invite' );

			Route::post( 'accept', [ 'uses' => 'InviteController@accept', 'middleware' => 'auth' ] );
			Route::post( 'refuse', 'InviteController@refuse' );
		} );

		Route::get( 'language/{locale}', 'LocaleController@change' );

		Route::group( [ 'prefix' => 'me', 'middleware' => 'auth' ], function () {
			Route::get( 'profile', 'AccountController@getProfile' );
			Route::post( 'profile', 'AccountController@postProfile' );
			Route::get( 'security', 'AccountController@getSecurity' );
			Route::post( 'security', 'AccountController@postSecurity' );
			Route::post( 'twofactor', 'AccountController@postTwoFactor' );
			Route::delete( 'twofactor', 'AccountController@deleteTwoFactor' );
		} );

		Route::group( [ 'prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'committee' ], function () {

			Route::get( '/', 'DashboardController@view' );

			// Session Types
			Route::resource( 'session-type', 'SessionTypeController', [
				'only' => [
					'index',
					'store',
					'update',
					'destroy'
				]
			] );

			// Email
			Route::group( [ 'prefix' => 'email' ], function () {

				Route::get( '/', 'EmailController@index' );
				Route::get( 'compose', 'EmailController@compose' );
				Route::post( 'send', 'EmailController@send' );

			} );

			//Organizing committee
			\App\Http\Controllers\Admin\OrganizingCommitteeController::routes();

			// Settings
			Route::get( 'settings/conference', 'SettingsController@show' );
			Route::post( 'settings/conference', 'SettingsController@store' );

		} );

	} );

}

/*
 * Routes needed for network admin.
 */
function admin_routes() {
	Route::group( [ 'namespace' => 'Network', 'middleware' => 'locale' ], function () {
		// Authentication Routes...
		Route::get( 'login', 'Auth\AuthController@showLoginForm' );
		Route::post( 'login', 'Auth\AuthController@login' );
		Route::get( 'twofactor', 'Auth\AuthController@showTwoFactorForm' );
		Route::post( 'twofactor', 'Auth\AuthController@twoFactor' );
		Route::get( 'logout', 'Auth\AuthController@logout' );

		// Password Reset Routes...
		Route::get( 'password/reset/{token?}', 'Auth\PasswordController@showResetForm' );
		Route::post( 'password/email', 'Auth\PasswordController@sendResetLinkEmail' );
		Route::post( 'password/reset', 'Auth\PasswordController@reset' );

		Route::group( [ 'prefix' => 'admin', 'middleware' => 'auth' ], function () {
			Route::get( '/', 'DashboardController@view' );
			Route::resource( 'conference', 'ConferenceController' );
			Route::resource( 'user', 'UserController', [ 'except' => [ 'show' ] ] );
			Route::group( [ 'prefix' => 'account' ], function () {
				Route::get( 'profile', 'AccountController@getProfile' );
				Route::post( 'profile', 'AccountController@postProfile' );
				Route::get( 'security', 'AccountController@getSecurity' );
				Route::post( 'security', 'AccountController@postSecurity' );
				Route::post( 'twofactor', 'AccountController@postTwoFactor' );
				Route::delete( 'twofactor', 'AccountController@deleteTwoFactor' );
			} );
		} );
	} );
}

Route::group( [ 'middleware' => 'web' ], function () {

	Route::group( [ 'prefix' => 'install' ], function () {
		Route::get( '/', 'InstallerController@index' );
		Route::get( 'form', 'InstallerController@form' );
		Route::post( 'form', 'InstallerController@store' );
		Route::get( 'success', 'InstallerController@success' );
	} );

	Route::group( [ 'prefix' => 'media' ], function () {
		Route::post( 'upload', 'FileController@upload' );
		Route::get( '{name}', 'FileController@get' )->where( 'name', '(.*)' );
	} );

	if ( Config::get( 'econf.multi' ) ) {
		// Multi
		Route::get( '/', function () {
			return view( 'welcome' );
		} );

		Route::get( 'manifest.json', 'Network\ManifestController@get' );

		admin_routes();

		Route::group( [ 'prefix' => '{conf_slug}', 'middleware' => 'multi' ], function () {
			conference_routes();
		} );

	} else {
		// Single
		Setting::setExtraColumns( array(
			TenantScope::$tenant_col => null
		) );
		conference_routes();
	}


} );