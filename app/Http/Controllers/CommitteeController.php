<?php

namespace App\Http\Controllers;

use App\Committee;
use App\Invite;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Str;
use Route;
use Setting;
use Mail;
use Flash;

class CommitteeController extends Controller {
	const PER_PAGE = 30;

	/**
	 * CommitteeController constructor.
	 */
	public function __construct() {
		$this->middleware( 'chair:' . $this->getCommitteeName() );
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$committee = $this->getCommittee();

		$class = get_called_class();

		$users = $committee->users()->paginate( self::PER_PAGE );

		return view( 'admin.committee', compact( 'committee', 'class', 'users' ) );

	}

	public function addUser( Request $request ) {

		$committee = $this->getCommittee();

		$emails = explode( PHP_EOL, $request->email );

		array_walk( $emails, function ( &$val, $idx ) {
			$val = trim( $val );
		} );

		foreach ( $emails as $email ) {

			if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) !== false ) {

				$is_part = $committee->users->contains( 'email', $email );

				if ( $is_part ) {

					$user = User::whereEmail( $email )->first();

					Flash::warning( trans( 'econf.committee.is_part', [ 'user' => $user->short_name ] ) );

				} else {

					$invite_data = [
						'type' => 'committee',
						'committee' => $committee->id,
						'chair' => $request->has( 'chair' ),
					];

					Invite::make( $email, $invite_data, 'econf.committee.invite_email', $request->message, function () use ( $committee ) {
						return [
							'committee' => $committee->full_name()
						];
					} );

				}
			}
		}

		Flash::success( trans( 'econf.committee.invite_sent' ) );

		return redirect( m_action( '\\' . get_called_class() . '@index' ) );

	}

	public function removeUser( Request $request, $id ) {
		$committee = $this->getCommittee();

		$user = User::find( $id );
		if ( !$committee->users->contains( $user ) ) {
			Flash::error( trans( 'econf.committee.user_not_part', [ 'name' => $user->name ] ) );
			return back();
		}

		$committee->users()->detach( $id );
		Flash::success( trans( 'econf.committee.remove_user_success', [ 'name' => $user->name ] ) );
		return redirect( m_action( '\\' . get_called_class() . '@index' ) );
	}

	public function makeChair( Request $request, $id ) {
		$committee = $this->getCommittee();

		$user = User::find( $id );
		if ( !$committee->users->contains( $user ) ) {
			Flash::error( trans( 'econf.committee.user_not_part', [ 'name' => $user->name ] ) );
			return back();
		}

		$user = User::find( $id );
		$committee->users()->updateExistingPivot( $user->id, [ 'chair' => true ], false );

		Flash::success( trans( 'econf.committee.make_chair_success', [ 'name' => $user->name ] ) );
		return redirect( m_action( '\\' . get_called_class() . '@index' ) );
	}

	public function removeChair( Request $request, $id ) {
		$committee = $this->getCommittee();

		$user = User::find( $id );
		if ( !$committee->users->contains( $user ) ) {
			Flash::error( trans( 'econf.committee.user_not_part', [ 'name' => $user->name ] ) );
			return back();
		}

		$user = User::find( $id );
		$committee->users()->updateExistingPivot( $user->id, [ 'chair' => false ], false );

		Flash::success( trans( 'econf.committee.remove_chair_success', [ 'name' => $user->name ] ) );
		return redirect( m_action( '\\' . get_called_class() . '@index' ) );
	}

	/**
	 * @return string The committee name (slug)
	 */
	public static function getCommitteeName() {
		return property_exists( get_called_class(), 'committee_name' ) ? static::$committee_name : '';
	}

	/**
	 * @return \App\Committee The committee
	 */
	public static function getCommittee() {
		return Committee::firstOrCreate( [ 'name' => static::getCommitteeName(), 'trans_key' => static::$trans_key ] );
	}

	public static function routes() {
		Route::group( [ 'prefix' => self::getCommitteeName() ], function () {
			Route::get( '/', '\\' . get_called_class() . '@index' );
			Route::put( '/', '\\' . get_called_class() . '@addUser' );
			Route::delete( '{id}', '\\' . get_called_class() . '@removeUser' );
			Route::put( '{id}/chair', '\\' . get_called_class() . '@makeChair' );
			Route::delete( '{id}/chair', '\\' . get_called_class() . '@removeChair' );
		} );
	}

	public static function emailGroup() {

		$committee = static::getCommittee();

		return [
			'id' => $committee->name,
			'name' => $committee->full_name(),
			'emails' => function () use ( $committee ) {
				$users = $committee->users;

				$list = [ ];

				foreach ( $users as $user ) {
					$item = [
						'id' => 'u' . $user->id,
						'name' => $user->name,
						'email' => $user->email,
						'toString' => sprintf( "%s &lt;%s&gt;", $user->name, $user->email )
					];
					$list[$item['id']] = $item;
				}

				return $list;
			}
		];
	}
}
