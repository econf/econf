<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Setting;
use App;

class ManifestController extends Controller
{
	public function get(){

		$manifest = [];
		$manifest['name'] = Setting::get('conf-name');

		if(Setting::has('conf-short_name')){
			$manifest['short_name'] = Setting::get('conf-short_name');
		}

		$manifest['start_url'] = m_action('PublicController@index');
		$manifest['display'] = "standalone";
		$manifest['lang'] = App::getLocale();

		if(config('econf.multi')){
			$manifest['scope'] = url("/".config('econf.conference.slug')."/");
		}

		$manifest['background_color'] = "#f7f7f7";

		if(Setting::has('conf-color')){
			$manifest['theme_color'] = Setting::get('conf-color');
		}

		if(Setting::has('conf-icon')){
			$manifest['icons'] = [ ['src' => action('FileController@get', Setting::get('conf-icon')) ]];
		}

		return $manifest;
	}
}
