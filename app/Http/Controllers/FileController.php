<?php

namespace App\Http\Controllers;

use App\Committee;
use App\Conference;
use App\Scopes\TenantScope;
use Hoa\Mime\Mime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Storage;
use File;
use Setting;
use Auth;
use Eventy;

class FileController extends Controller
{
	//
	public function get($name){

		$name = trim($name, '/');

		$path = explode('/',$name);

		if(count($path) == 1){
			abort(404);
		}

		if($this->canAccess($path)){

			if(config('econf.multi')){
				$slug = array_shift($path);
			}
			
			$new_path = Eventy::filter('file.transform_name', $path);

			if(config('econf.multi')){
				array_unshift($new_path, $slug);
			}
			
			$name = implode('/', $new_path);
			
			if(Storage::exists($name)){
				$file = Storage::get($name);
				$namea = explode('.',$name);
				$extension = end($namea);
				$extension = strtolower($extension);
				$mime = Mime::getMimeFromExtension($extension);
				return response($file, 200)->header('Content-Type', $mime);
			} 
		} 
		
		abort(404);

	}

	private function canAccess($path){
		if(count($path) == 1){
			return false;
		}

		if($path[0] == 'public'){
			return true;
		}

		if(config('econf.multi')){
			//Check conference
			$slug = array_shift($path);
			$conference = Conference::where('slug', $slug)->first();
			if(empty($conference)){
				return false;
			}
			config( [ 'econf.conference.id' => $conference->id, 'econf.conference.slug' => $conference->slug ] );

			Setting::setExtraColumns(array(
				TenantScope::$tenant_col => $conference->id
			));
		}

		if($path[0] == 'public'){
			return true;
		}

		if(Committee::in('organizing')){
			return true;
		}

		return Eventy::filter('file.can', false, $path);
	}

	public function upload(Request $request){
		$file = $request->file('file');

		$fname = $file->getClientOriginalName();

		// Check duplicates

		if(Storage::exists($request->dir.'/'.$file->getClientOriginalName())){
			$extension = $file->getClientOriginalExtension();
			$name = basename($fname, ".".$extension);
			$fname = $name." C.".$extension;
		}

		Storage::put($request->dir.'/'.$fname, File::get($file));
	}


}