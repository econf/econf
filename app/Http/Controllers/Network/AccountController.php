<?php

namespace App\Http\Controllers\Network;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Google2FA;
use Auth;
use Flash;
use Validator;

class AccountController extends Controller
{
    public function getProfile(){
		return view('network.account.profile');
    }

	public function postProfile(Request $request){
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users,email,'.Auth::id().',id,conference_id,NULL',
		]);

		$user = Auth::user();
		$user->name = $request->name;
		$user->email = $request->email;
		$user->save();

		Flash::success(trans('econf.user.profile_success'));

		return redirect(action('Network\AccountController@getProfile'));

	}

	public function getSecurity(){
		$token = Google2FA::generateSecretKey();
		$g2fa_url = Google2FA::getQRCodeGoogleUrl(
			'E-Conf',
			Auth::user()->email,
			$token
		);
		return view('network.account.security', compact('token', 'g2fa_url'));
	}

	public function postSecurity(Request $request){

		$validator = Validator::make($request->all(), [
			'current_password' => 'required',
			'new_password' => 'required|confirmed|min:6'
		]);

		$validator->after(function($validator) use ($request) {
			if(!empty($request->current_password)) {
				$check = auth()->validate( [
					'email' => Auth::user()->email,
					'password' => $request->current_password
				] );
				if ( !$check ):
					$validator->errors()->add( 'current_password',
						trans( 'econf.user.current_password_incorrect' ) );
				endif;
			}
		});
		if ($validator->fails()):
			return redirect(action('Network\AccountController@getSecurity'))
				->withErrors($validator)
				->withInput();
		endif;

		$user = Auth::user();
		$user->password = bcrypt($request->new_password);
		$user->save();

		Flash::success(trans('econf.user.change_password_success'));

		return redirect(action('Network\AccountController@getSecurity'));
	}

	public function postTwoFactor(Request $request){

		// Validate
		$this->validate($request, [
			'token' => 'required',
			'code' => 'required'
		]);

		$valid = Google2FA::verifyKey($request->token, $request->code);

		if($valid){
			/**
			 * @var \App\User $user
			 */
			$user = Auth::user();
			$user->g2fa_key = $request->token;
			$user->save();

			Flash::success(trans('econf.user.2fa.enabled_success'));
		} else {
			Flash::error(trans('econf.user.2fa.wrong_code'));
		}

		return redirect(action('Network\AccountController@getSecurity'));
	}

	public function deleteTwoFactor(Request $request){

		// Validate
		$this->validate($request, [
			'code' => 'required'
		]);

		$user = Auth::user();

		$valid = Google2FA::verifyKey($user->g2fa_key, $request->code);

		if($valid){
			$user->g2fa_key = null;
			$user->save();
			Flash::success(trans('econf.user.2fa.disabled_success'));
		} else {
			Flash::error(trans('econf.user.2fa.wrong_code'));
		}

		return redirect(action('Network\AccountController@getSecurity'));

	}
}
