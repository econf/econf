<?php

namespace App\Http\Controllers\Network;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App;

class ManifestController extends Controller
{
    public function get(){
	    $manifest = [];
	    $manifest['name'] = "E-Conf";
	    $manifest['start_url'] = url('/');
	    $manifest['display'] = "standalone";
	    $manifest['icons'] = [[ 'src' => asset('images/favicon.png')]];
	    $manifest['background_color'] = "#f7f7f7";
	    $manifest['lang'] = App::getLocale();
	    $manifest['theme_color'] = "#0087C3";

	    return $manifest;
    }
}
