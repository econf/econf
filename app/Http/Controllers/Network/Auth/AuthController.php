<?php

namespace App\Http\Controllers\Network\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Auth;
use Session;
use Google2FA;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesUsers, ThrottlesLogins;

    protected $loginView = 'network.auth.login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * @inheritDoc
     */
    public function redirectPath() {
        return action('Network\DashboardController@view');
    }

    /**
     * @inheritDoc
     */
    public function login( Request $request ) {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if(Auth::validate($credentials)){
            // Valid
            $user = User::where('email', $request->email)->first();

            if($user->g2fa_key){
                Session::put('id', $user->id);
                Session::put('remember', $request->has('remember'));

                return redirect(action('Network\Auth\AuthController@showTwoFactorForm'));
            }

            Auth::login($user, $request->has('remember'));
            return $this->handleUserWasAuthenticated($request, $throttles);

        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);


    }

    public function showTwoFactorForm(){
        if(!Session::has('id')){
            return redirect(action('Network\Auth\AuthController@showLoginForm'));
        }

        $user = User::find(Session::get('id'));
        return view('network.auth.twofactor', compact('user'));
    }

    public function twoFactor(Request $request){
        if(!Session::has('id')){
            return redirect(action('Network\Auth\AuthController@showLoginForm'));
        }

        $user = User::find(Session::get('id'));
        $valid = Google2FA::verifyKey($user->g2fa_key, $request->code);

        if($valid){
            Auth::login($user, Session::get('remember', false));
            Session::remove('id');
            Session::remove('remember');
            return $this->handleUserWasAuthenticated($request, $this->isUsingThrottlesLoginsTrait());
        }

        return redirect()->back()
                         ->withErrors([
                             'code' => trans('econf.user.2fa.wrong_code'),
                         ]);

    }


}
