<?php

namespace App\Http\Controllers\Network;

use App\User;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Mail;

class UserController extends Controller
{
    const PER_PAGE = 20;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name')->paginate( self::PER_PAGE );
        return view('network.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('network.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,NULL,id,conference_id,NULL',
        ]);

        // Generate user password
        $password = Str::random(8);

        // Create user
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($password);
        $user->conference_id = null;
        $user->save();

        $data = [
            'user_name' => $request->name,
            'user_email' => $request->email,
            'password' => $password,
            'url' => action('Network\Auth\AuthController@showLoginForm'),
        ];

        // Send email
        Mail::send('network.emails.new_user', $data, function($m) use ($request){
            $m->to($request->email)->subject(trans('econf.network.new_user_email.subject'));
        });

        Flash::success( trans( 'econf.network.new_user_success' ) );

        return redirect(action('Network\UserController@index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('network.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // Validate
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$user->id.',id,conference_id,NULL',
        ]);

        // Save
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        // Add message and redirect
        Flash::success( trans( 'econf.network.edit_user_success' ) );

        return redirect(action('Network\UserController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        Flash::success( trans( 'econf.network.delete_user_success' ) );
        return redirect(action('Network\UserController@index'));
    }
}
