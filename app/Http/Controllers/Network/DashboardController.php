<?php

namespace App\Http\Controllers\Network;

use App\Conference;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //
	function view(){
		$conf_count = Conference::count();
		$user_count = User::allTenants()->count();

		return view('network.dashboard', compact('conf_count', 'user_count'));
	}
}
