<?php

namespace App\Http\Controllers\Network;

use App\Committee;
use App\Conference;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

use DB;
use Mail;
use Flash;

class ConferenceController extends Controller
{

    const PER_PAGE = 20;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conferences = Conference::paginate(self::PER_PAGE);
        return view('network.conference.index', compact('conferences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('network.conference.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate
        $this->validate($request, [
            'slug' => 'required|unique:conferences',
            'name' => 'required',
            'user_name' => 'required',
            'user_email' => 'required|email'
        ]);

        // Create conference
        $conference = new Conference();
        $conference->slug = $request->slug;
        $conference->save();

        // Store name
        DB::table('settings')->insert(
            ['key' => 'conf-name', 'value' => $request->name, 'conference_id' => $conference->id]
        );

        // Generate user password
        $password = Str::random(8);

        // Create user
        $user = new User();
        $user->name = $request->user_name;
        $user->email = $request->user_email;
        $user->password = bcrypt($password);
        $user->conference_id = $conference->id;
        $user->save();

        $data = [
            'user_name' => $request->user_name,
            'user_email' => $request->user_email,
            'password' => $password,
            'conference_name' => $request->name,
            'conference_url' => action('PublicController@index', $conference->slug)
        ];

        // Assign to organizing committee
        $committee = Committee::allTenants()->firstOrCreate([
            'name' => 'organizing',
            'conference_id' => $conference->id,
        ]);
        $committee->users()->attach($user->id, ['chair' => true]);

        // Send email
        Mail::send('network.emails.new_conference', $data, function($m) use ($request){
            $m->to($request->user_email)->subject(trans('econf.network.new_conf_email.subject'));
        });

        Flash::success( trans( 'econf.network.new_conference_success' ) );

        return redirect(action('Network\ConferenceController@index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function show(Conference $conference)
    {
        $users = User::allTenants()->whereIn('id', function($query) use ($conference){
            $query->select('user_id')->from('committee_user')->where('committee_id', function($query) use ($conference){
                $query->select('id')->from('committees')->where('conference_id', $conference->id)->where('name', 'organizing');
            });
        })->orderBy('name')->get();
        return view('network.conference.show', compact('conference', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function edit(Conference $conference)
    {
        return view('network.conference.edit', compact('conference'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conference $conference)
    {
        // Validate
        $this->validate($request, [
            'slug' => 'required|unique:conferences,slug,'.$conference->id,
        ]);

        // Save
        $conference->slug = $request->slug;
        $conference->save();

        // Add message and redirect
        Flash::success( trans( 'econf.network.edit_conference_success' ) );

        return redirect(action('Network\ConferenceController@show', $conference));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conference $conference)
    {
        $conference->delete();
        Flash::success( trans( 'econf.network.delete_conference_success' ) );
        return redirect(action('Network\ConferenceController@index'));
    }
}
