<?php

namespace App\Http\Controllers;

use App\Invite;
use App\User;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;
use Session;

class InviteController extends Controller
{
    //
	public function invite($token){

		$invite = Invite::where('token', $token)->firstOrFail();
		$data = compact('invite');

		if(Auth::check()){
			return view('invites.logged', $data);
		} else {
			// Save this URL to return later
			Session::put('url.intended', url()->full());
			return view('invites.guest', $data);
		}
	}

	public function accept($token){

		$invite = Invite::where('token', $token)->firstOrFail();

		\Eventy::action('invite.accept.'.$invite->data('type'), $invite);

		$this->sendFeedbackEmail($invite, true);

		$invite->delete();

		return redirect(m_action('PublicController@index'));

	}

	public function refuse($token){

		$invite = Invite::where('token', $token)->firstOrFail();
		$this->sendFeedbackEmail($invite, false);
		$invite->delete();

		Session::remove('url.intended');

		// TODO: Notify someone?

		\Flash::info(trans('econf.invite.refused_success'));

		return redirect(m_action('PublicController@index'));
	}

	private function sendFeedbackEmail(Invite $invite, $accepted){

		$inviter = User::find($invite->data('inviter_id'));

		if(empty($inviter)){
			return;
		}

		$data = [
			'invitee_name' => (Auth::check())?(Auth::user()->name):$invite->email,
			'invite' => $invite,
			'trans_ns' => $accepted?'econf.invite.accepted_email':'econf.invite.refused_email'
		];

		scope_locale($inviter->data('locale', config('app.locale')), function() use ($inviter, $data){

			Mail::send( 'invites.emails.feedback', $data, function ( $m ) use ( $inviter, $data ) {
				$m->to( $inviter->email )->subject( trans( $data['trans_ns'].'.subject', ['name' => $data['invitee_name']] ) );
			} );
		});
	}
	
}
