<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Eventy;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Mail;
use Setting;

class EmailController extends Controller {

	//
	public function index() {

		$groups = $this->getGroups();

		$templates = $this->getTemplates();

		return view( 'admin.email.index', compact( 'groups', 'templates' ) );

	}

	public function compose( Request $request ) {

		if ( !$request->has( 'group' ) || !$this->getGroups()->keys()->contains($request->group) ) {
			return redirect( m_action( 'Admin\EmailController@index' ) );
		}

		$template = $this->getTemplates()[$request->get('template', 'blank')];

		if(!empty($template['view'])){
			$template['body'] = view($template['view'])->render();
		}

		$group = $this->getGroups()[$request->group];
		$func = $group['emails'];
		$members = collect($func());

		$vars = Arr::only($this->getVariables(), array_keys(array_merge($members->first(), $this->getConfVarValues())));

		return view( 'admin.email.compose', compact( 'group', 'members', 'vars', 'template' ) );

	}

	public function send( Request $request ) {

		// TODO: Improve error handling

		$groups = $this->getGroups();

		if(!array_key_exists($request->group, $groups->all())){
			return back();
		}

		$group = $groups[$request->group];

		$all_emails = $group['emails']();

		if($request->has('recipients')){
			$emails = Arr::only($all_emails, $request->recipients);
		} else {
			$emails = $all_emails;
		}

		foreach($emails as $email){
			$data = array_merge($this->getConfVarValues(), $email);
			$vars = collect($data)->flip()->map(function($item, $key){
				return '[:'.$item.':]';
			})->flip();

			list($message, $subject) = str_replace($vars->keys()->all(), $vars->values()->all(), [$request->message, $request->subject]);

			Mail::send('emails.empty', ['content' => $message], function($m) use ($email, $subject){
				$m->to($email['email'], $email['name'])->subject($subject);
			});
		}

		Flash::success('Emails sent successfully');
		return redirect(m_action('Admin\EmailController@index'));
	}

	/**
	 * @return \Illuminate\Support\Collection
	 */
	public function getGroups() {


		$appGroups = [
			'all_users' => [
				'id' => 'all_users',
				'name' => trans('econf.admin.email.all_users'),
				'emails' => function () {
					$users = User::orderBy( 'name' )->get();

					$list = [ ];

					foreach ( $users as $user ) {
						$item = [
							'id' => 'u' . $user->id,
							'name' => $user->name,
							'email' => $user->email,
							'toString' => sprintf( "%s &lt;%s&gt;", $user->name, $user->email )
						];
						$list[$item['id']] = $item;
					}

					return $list;
				}
			]
		];

		$ocg = OrganizingCommitteeController::emailGroup();
		$appGroups[$ocg['id']] = $ocg;

		$groups = collect( Eventy::filter( 'admin.email.groups', $appGroups ) );

		return $groups;

	}

	public function getGroupEmails( $group ) {
		$func = $this->getGroups()[$group]['emails'];
		return collect( $func() );
	}

	public function getVariables() {

		$appVars = trans('econf.admin.email.vars');

		$vars = Eventy::filter( 'admin.email.vars', $appVars );

		return $vars;

	}

	public function getConfVarValues(){
		return [
			'confName' => Setting::get('conf-name'),
			'confShortName' => Setting::get('conf-short_name', Setting::get('conf-name')),
		];
	}

	public function getTemplates(){

		/*
		 * Template format
		 * [ 'welcome' => [
		 *      'id' => 'welcome',
		 *      'name' => 'Welcome email',
		 *      'subject' => 'Welcome to [:conf_name:]',
		 *      'view' => 'emails.template.welcome',    // View or body. View takes precedence.
		 *      'body' => '',
		 * ]]
		 */

		$appTemplates = [
			'blank' => [
				'id' => 'blank',
				'name' => trans('econf.admin.email.blank_template'),
				'subject' => '',
				'body' => ''
			]
		];

		$templates = collect( Eventy::filter( 'admin.email.templates', $appTemplates ) );

		return $templates;

	}
}
