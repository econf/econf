<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Date;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Eventy;
use Setting;

class DashboardController extends Controller
{
    //
	public function view(){

		$data = [
			[
				'label' => trans('econf.dashboard.users'),
				'value' => User::count(),
				'icon' => 'fa fa-group',
			]
		];

		$data = Eventy::filter('dashboard.data', $data);

		$calendar = [ ];

		if( Setting::has('conf-start_date') && Setting::has('conf-end_date')){
			$calendar[] = [
				'label' => trans('econf.admin.conference'),
				'start' => Date::parse(Setting::get('conf-start_date')),
				'end' => Date::parse(Setting::get('conf-end_date')),
			];
		}

		$calendar = Eventy::filter('dashboard.calendar', $calendar);

		usort($calendar, function($a, $b){
			if($b['start']->gt($a['start'])){
				return -1;
			}
			if($b['start']->lt($a['start'])){
				return 1;
			}
			return 0;
		});

		return view('admin.dashboard', compact('data', 'calendar'));
	}
}
