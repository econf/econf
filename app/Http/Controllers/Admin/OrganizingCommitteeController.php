<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 05/04/16
 * Time: 00:53
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\CommitteeController;

class OrganizingCommitteeController extends CommitteeController{

	protected static $committee_name = 'organizing';
	protected static $trans_key = 'econf.admin.organizing_committee';

}