<?php

namespace App\Http\Controllers\Admin;

use App\Committee;
use App\SessionType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Flash;

class SessionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.session_types');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Committee::needs('organizing');

        $this->validate($request, [
            'name' => 'required',
            'duration' => 'required|integer|min:0'
        ]);

        SessionType::create($request->all());

        Flash::success(trans('econf.session_types.create_success'));

        return redirect(m_action('Admin\SessionTypeController@index'));
    }

    public function update(Request $request, $id){
        Committee::needs('organizing');

        $this->validate($request, [
            'name' => 'required',
            'duration' => 'required|integer|min:0'
        ]);

        $session_type = SessionType::findOrFail($id);

        $session_type->update([
            'name' => $request->name,
            'duration' => $request->duration,
            'parallel' => $request->exists('parallel')
        ]);

        $session_type->save();

        Flash::success(trans('econf.session_types.update_success'));

        return redirect(m_action('Admin\SessionTypeController@index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Committee::needs('organizing');

        $session_type = SessionType::findOrFail($id);

        $session_type->delete();

        Flash::success(trans('econf.session_types.delete_success'));

        return redirect(m_action('Admin\SessionTypeController@index'));
    }
}
