<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Topic;
use File;
use Flash;
use Illuminate\Http\Request;
use Setting;
use Storage;

class SettingsController extends Controller {
	//
	public function show() {

		$topics = Topic::orderBy('name')->get()->implode( 'name', PHP_EOL );

		return view( 'admin.settings', compact( 'topics' ) );
	}


	public function store( Request $request ) {

		// Validate request
		$this->validate( $request, [
			'setting.conf-name' => 'required',
			'setting.conf-start_date' => 'date_format:Y-m-d',
			'setting.conf-end_date' => 'date_format:Y-m-d|after:setting.conf-start_date',
			'setting.conf-color' => 'hexcolor',
		] );

		// Save settings
		$setting = $request->setting;
		foreach ( $setting as $key => $value ) {
			if ( trim( $value ) == false ) {
				Setting::forget( $key );
			} else {
				Setting::set( $key, trim( $value ) );
			}
		}

		// Save conference topics
		Topic::sync( explode( PHP_EOL, $request->topics ) );

		// Save logos

		$images = ['logo', 'logo_white', 'icon'];

		foreach ($images as $img){
			if ( $request->hasFile( $img ) ) {
				$image = $request->file( $img );
				$name = uniqid() . '.' . $image->getClientOriginalExtension();
				$path = m_path( 'public/' . $name );
				$result = Storage::put( $path, File::get( $image ) );
				if ( $result ) {
					Setting::set( "conf-{$img}", $path );
				}
			}
		}

		// Save color and generate CSS
		if ( !empty( $setting['conf-color'] ) ) {
			$color = $setting['conf-color'];
			$filename = 'public/skins/skin-' . trim( $color, '#' ) . '.css';
			if ( !Storage::exists( $filename ) ) {
				$parser = new \Less_Parser();
				$parser->parseFile( base_path( 'resources/assets/less/skin.less' ), base_path( 'resources/assets/less' ) );
				$parser->ModifyVars( [ 'color' => $color ] );
				$css = $parser->getCss();
				Storage::put( $filename, $css );
			}
			$public_filename = 'public/skins/triangle-' . trim( $color, '#' ) . '.css';
			if ( !Storage::exists( $public_filename ) ) {
				$parser = new \Less_Parser();
				$parser->parseFile( public_path( 'components/triangle/less/main.less' ), asset( 'components/triangle/less' ) );
				$parser->ModifyVars( [ 'main-color' => $color ] );
				$css = $parser->getCss();
				Storage::put( $public_filename, $css );
			}
		}

		Flash::success( trans( 'econf.admin.settings.conference.success' ) );

		return redirect( m_action( 'Admin\SettingsController@show' ) );

	}
}
