<?php

namespace App\Http\Controllers;

use Auth;
use Cookie;

use App\Http\Requests;
use Session;

class LocaleController extends Controller
{
    //
	public function change($locale){
		
		if(in_array($locale, array_keys(config('languages')))){
			if(Auth::check()){
				$user = Auth::user();
				$data = $user->data;
				$data['locale'] = $locale;
				$user->data = $data;
				$user->save();
			} else {
				Session::put('locale', $locale);
			}
		}

		return back();
	}
}
