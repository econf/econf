<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Admin\OrganizingCommitteeController;
use App\Http\Middleware\LocaleMiddleware;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Setting;

class InstallerController extends Controller {

	public function index( Request $request ) {
		if ( Setting::get( 'installed', false ) ) {
			return redirect( '/' );
		}

		$this->setLocale( $request );

		return view( 'install.index' );

	}

	public function form(Request $request) {
		if ( Setting::get( 'installed', false ) ) {
			return redirect( '/' );
		}

		$this->setLocale( $request );

		return view( 'install.form' );

	}

	public function store( Request $request ) {
		if ( Setting::get( 'installed', false ) ) {
			return redirect( '/' );
		}

		$rules = [
			'name' => 'required',
			'email' => 'required',
			'password' => 'required|confirmed'
		];

		if ( !config( 'econf.multi' ) ) {
			$rules['conf_name'] = 'required';
		}

		$this->validate( $request, $rules );

		if ( !config( 'econf.multi' ) ) { // Set conference name
			Setting::set( 'conf-name', $request->conf_name );
		}

		// Create user
		$user = new User();
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = bcrypt( $request->password );
		$user->conference_id = null;
		$user->save();

		if(!config('econf.multi')){
			// Assign to organizing committee
			$committee = OrganizingCommitteeController::getCommittee();
			$committee->users()->attach($user->id, ['chair' => true]);
		}

		return redirect( action( 'InstallerController@success' ) );

	}

	public function success(Request $request) {
		if ( Setting::get( 'installed', false ) ) {
			return redirect( '/' );
		}

		if ( User::count() == 0 ) {
			return redirect( action( 'InstallerController@index' ) );
		}

		$this->setLocale( $request );

		Setting::set( 'installed', true );

		$url = url( 'login' );
		return view( 'install.success', compact( 'url' ) );
	}

	private function setLocale( Request $request ) {
		if ( $request->has( 'locale' ) ) {
			LocaleMiddleware::setLocale( $request->get( 'locale' ) );
			return;
		} else {
			$header = $request->server('HTTP_ACCEPT_LANGUAGE');
			$languages = collect(explode(',', $header));
			$languages->transform( function ($item, $key){
				$exploded = explode(';', $item);
				return $exploded[0];
			});
			$available_languages = array_keys( config('languages'));
			foreach ($languages as $language){
				if(in_array( $language, $available_languages)){
					LocaleMiddleware::setLocale( $language );
					return;
				}
			}
		}
	}


}
