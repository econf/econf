<?php

namespace App\Http\ViewComposers;

use App;
use App\Committee;
use Eventy;
use Illuminate\View\View;
use Menu;
use Asset;
use Setting;
use Storage;

class AdminComposer {
	/**
	 * Create a new network admin composer.
	 *
	 * @return void
	 */
	public function __construct() {

		$this->buildMenu();

		$this->addAssets();

	}

	/**
	 * Bind data to the view.
	 *
	 * @param  View $view
	 * @return void
	 */
	public function compose( View $view ) {

	}

	private function buildMenu() {

		Eventy::addAction( 'admin.menu', function ( $menu ) {
			$menu->raw( trans( 'econf.admin.conference' ), [ 'class' => 'header' ] );
		}, 15, 1 );

		Menu::make( 'AdminMenu', function ( $menu ) {

			$menu->add( trans( 'econf.admin.view_public_page' ), m_action( 'PublicController@index' ) )->prepend( '<span class="fa fa-home"></span>' );

			Eventy::action( 'admin.menu', $menu );

			if ( Committee::chair() ) {
				$menu->raw( trans( 'econf.admin.organization' ), [ 'class' => 'header' ] );

				$menu->add( trans( 'econf.admin.email.label' ), m_action( 'Admin\EmailController@index' ) )->prepend( '<span class="fa fa-envelope"></span>' );

				if ( Committee::in( 'organizing', true ) ) {
					$menu->add( trans( 'econf.admin.organizing_committee' ), m_action( 'Admin\OrganizingCommitteeController@index' ) )->prepend( '<span class="fa fa-suitcase"></span>' );
				}

				// Hook here
				Eventy::action( 'admin.menu.organizer', $menu );

				if ( Committee::in( 'organizing' ) ) {
					$settings_menu = $menu->add( trans( 'econf.admin.settings.label' ) )->prepend( '<span class="fa fa-cog"></span>' );

					$settings_menu->add( trans( 'econf.admin.settings.conference.label_short' ), m_action( 'Admin\SettingsController@show' ) )->prepend( '<span class="fa fa-calendar"></span>' );
					$settings_menu->add( trans( 'econf.session_types.label' ), m_action( 'Admin\SessionTypeController@index' ) )->prepend( '<span class="fa fa-th-large"></span>' );

					// Menu Settings Action
					Eventy::action( 'admin.menu.settings', $settings_menu );
				}
			}

		} );
	}

	private function addAssets() {

		// jQuery
		Asset::add( 'bower_components/jquery/dist/jquery.min.js', 'footer' );

		// jQuery UI
		Asset::add( 'bower_components/jquery-ui/jquery-ui.min.js', 'footer' );

		// Bootstrap
		Asset::add( 'bower_components/bootstrap/dist/css/bootstrap.min.css' );
		Asset::add( 'bower_components/bootstrap/dist/js/bootstrap.min.js', 'footer' );

		// Font awesome
		Asset::add( 'bower_components/components-font-awesome/css/font-awesome.min.css' );

		// Bootstrap color picker
		Asset::add( 'bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css' );
		Asset::add( 'bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js', 'footer' );

		// Koala
		Asset::add( 'bower_components/koala/css/koalaeditor.min.css' );
		Asset::add( 'bower_components/koala/js/koalaeditor.min.js', 'footer' );
		if ( App::getLocale() != "en" ) {
			Asset::add( "bower_components/koala/js/langs/" . App::getLocale() . ".js", 'footer' );
		}

		// Select2
		Asset::add( 'bower_components/select2/dist/css/select2.min.css' );
		Asset::add( 'bower_components/select2/dist/js/select2.min.js', 'footer' );
		
		// Slimscroll
		Asset::add( 'bower_components/slimScroll/jquery.slimscroll.min.js', 'footer' );
		
		// Autogrow
		Asset::add( 'bower_components/autogrow/autogrow.js', 'footer' );
		
		// AdminLTE
		Asset::add( 'components/admin-lte/dist/css/AdminLTE.min.css' );
		if(Setting::has('conf-color') && Storage::exists('public/skins/skin-'.trim(Setting::get('conf-color'), '#').'.css')){
			Asset::add( action('FileController@get', 'public/skins/skin-'.trim(Setting::get('conf-color'), '#').'.css') );
		} else {
			Asset::add( 'components/admin-lte/dist/css/skins/skin-blue.min.css' );
		}
		Asset::add( 'components/admin-lte/dist/js/app.min.js', 'footer' );

		// Improvers
		Asset::add( 'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js', 'ie' );
		Asset::add( 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js', 'ie' );

		// App
		Asset::add( 'css/style.css' );
		Asset::add( 'js/admin.js', 'footer' );

		Asset::add(Eventy::filter( 'admin.assets', [] ));


	}
}