<?php

namespace App\Http\ViewComposers;

use App;
use App\Committee;
use App\Conference;
use App\Scopes\TenantScope;
use Asset;
use Eventy;
use Illuminate\View\View;
use Menu;
use Auth;
use Setting;
use Storage;

class ErrorComposer {

	/**
	 * @var Conference
	 */
	private $conference = null;

	/**
	 * Create a new error composer.
	 *
	 * @return void
	 */
	public function __construct() {
		return;
	}

	/**
	 * Bind data to the view.
	 *
	 * @param  View $view
	 * @return void
	 */
	public function compose( View $view ) {
		$c = [];
		if($this->isConference()){
			$c['name'] = Setting::get('conf-short_name', Setting::get('conf-name'));
			$c['logo'] = false;
			$c['stylesheet'] = asset('components/triangle/css/main.css');
			$c['home_url'] = m_action( 'PublicController@index' );

			if($view->getData()['code'] != 503 && Setting::has('conf-logo')){
				$c['logo'] = action( 'FileController@get', Setting::get('conf-logo') );
			}
			if($view->getData()['code'] != 503 && Setting::has('conf-color')){
				$public_filename = 'public/skins/triangle-' . trim( Setting::get('conf-color'), '#' ) . '.css';
				if ( Storage::exists( $public_filename ) ) {
					$c['stylesheet'] = action( 'FileController@get', $public_filename );
				}
			}

		} else {
			$c['name'] = 'E-Conf';
			$c['logo'] = asset( 'images/logo.png' );
			$c['stylesheet'] = asset('components/triangle/css/main.css');
			$c['home_url'] = url('/');
		}
		$view->with( 'conference', $c );
		return;
	}

	public function isConference(){

		if(config('econf.multi') == false){
			return true;
		}

		$segments = request()->segments();
		if(empty($segments)){
			$cs = "";
		} else {
			$cs = $segments[0];
		}
		if($cs == "admin"){
			return false;
		}

		$conference = Conference::where( 'slug', $cs )->first();

		if(empty($conference)){
			return false;
		}

		$this->conference = $conference;

		config( [ 'econf.conference.id' => $conference->id, 'econf.conference.slug' => $conference->slug ] );

		Setting::setExtraColumns(array(
			TenantScope::$tenant_col => $conference->id
		));

		return true;
	}

}