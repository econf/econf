<?php

namespace App\Http\ViewComposers;

use App;
use App\Committee;
use Asset;
use Eventy;
use Illuminate\View\View;
use Menu;
use Auth;
use Setting;
use Storage;

class PublicComposer {
	/**
	 * Create a new network admin composer.
	 *
	 * @return void
	 */
	public function __construct() {

		$this->buildMenu();
		$this->addAssets();

	}

	/**
	 * Bind data to the view.
	 *
	 * @param  View $view
	 * @return void
	 */
	public function compose( View $view ) {

	}

	private function buildMenu() {

		Eventy::addAction( 'public.menu', function ( $menu ) {

		}, 1, 1 );


		Eventy::addAction( 'public.menu', function ( $menu ) {

			if ( Auth::check() ) {
				$item = $menu->add( Auth::user()->short_name, null);
				Eventy::action( 'public.menu.user', $item );
				$item->add( trans( 'econf.user.profile' ), m_action( 'AccountController@getProfile' ) )->divide();
				if ( Committee::any() ) {
					$item->add( trans( 'econf.admin.label' ), m_action( 'Admin\DashboardController@view' ) )->divide();
				}
				$item->add( trans( 'econf.auth.logout' ), m_action( 'Auth\AuthController@logout' ) );
			} else {
				$menu->add( trans( 'econf.auth.login' ), m_action( 'Auth\AuthController@showLoginForm' ) );
			}

			$litem = $menu->add( strtoupper( App::getLocale() ));
			foreach ( config( 'languages' ) as $code => $name ) {
				$litem->add( $name, m_action( 'LocaleController@change', $code ) );
			}

		}, 99, 1 );

		Menu::make( 'PublicMenu', function ( $menu ) {

			Eventy::action( 'public.menu', $menu );

		} );
	}

	private function addAssets() {

		// jQuery
		Asset::add( 'bower_components/jquery/dist/jquery.min.js', 'footer' );

		// Bootstrap
		Asset::add( 'bower_components/bootstrap/dist/css/bootstrap.min.css' );
		Asset::add( 'bower_components/bootstrap/dist/js/bootstrap.min.js', 'footer' );

		// Font awesome
		Asset::add( 'bower_components/components-font-awesome/css/font-awesome.min.css' );

		// Select2
		Asset::add( 'bower_components/select2/dist/css/select2.min.css' );
		Asset::add( 'bower_components/select2/dist/js/select2.min.js', 'footer' );
		Asset::add( 'components/select2-themes/select2-bootstrap.min.css' );

		// Autogrow
		Asset::add( 'bower_components/autogrow/autogrow.js', 'footer' );

		// Improvers
		Asset::add( 'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js', 'ie' );
		Asset::add( 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js', 'ie' );

		// Triangle
		Asset::add( 'components/triangle/css/animate.min.css' );
		//Asset::add( 'components/triangle/css/main.css' );
		//Asset::add( 'components/triangle/css/responsive.css' );
		//Asset::add( 'components/triangle/js/wow.min.js' );

		if(Setting::has('conf-color') && Storage::exists('public/skins/triangle-'.trim(Setting::get('conf-color'), '#').'.css')){
			Asset::add( action('FileController@get', 'public/skins/triangle-'.trim(Setting::get('conf-color'), '#').'.css') );
		} else {
			Asset::add( 'components/triangle/css/main.css' );
		}

		// App
		Asset::add( 'css/public.css' );
		Asset::add( 'js/public.js', 'footer' );

		Asset::add(Eventy::filter( 'public.assets', [] ));


	}

}