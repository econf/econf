<?php

namespace App\Http\ViewComposers;

use Asset;
use Illuminate\View\View;

use Menu;

class NetworkAdminComposer {
	/**
	 * Create a new network admin composer.
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->buildMenu();
		$this->addAssets();

	}

	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{

	}

	private function buildMenu() {
		Menu::make( 'NetworkMenu', function ( $menu ) {
			$menu->raw( trans( 'econf.network.network' ), [ 'class' => 'header' ] );
			$menu->add( trans( 'econf.network.conferences' ), action('Network\ConferenceController@index') )->prepend( '<span class="fa fa-calendar"></span>' );
			$menu->add( trans( 'econf.network.users' ), action('Network\UserController@index') )->prepend( '<span class="fa fa-group"></span>' );


			$menu->raw( trans( 'econf.user.personal' ), [ 'class' => 'header' ] );
			$account = $menu->add( trans( 'econf.user.account' ), '#' )->prepend( '<span class="fa fa-user"></span>' );
			$account->add(trans('econf.user.profile'), action('Network\AccountController@getProfile'))->prepend( '<span class="fa fa-user"></span>' );
			$account->add(trans('econf.user.security'), action('Network\AccountController@getSecurity'))->prepend( '<span class="fa fa-lock"></span>' );
		});
	}

	private function addAssets() {

		// jQuery
		Asset::add( 'bower_components/jquery/dist/jquery.min.js', 'footer' );

		// Bootstrap
		Asset::add( 'bower_components/bootstrap/dist/css/bootstrap.min.css' );
		Asset::add( 'bower_components/bootstrap/dist/js/bootstrap.min.js', 'footer' );

		// Font awesome
		Asset::add( 'bower_components/components-font-awesome/css/font-awesome.min.css' );

		// Slimscroll
		Asset::add( 'bower_components/slimScroll/jquery.slimscroll.min.js', 'footer' );

		// AdminLTE
		Asset::add( 'components/admin-lte/dist/css/AdminLTE.min.css' );
		Asset::add( 'components/admin-lte/dist/css/skins/skin-blue.min.css' );
		Asset::add( 'components/admin-lte/dist/js/app.min.js', 'footer' );

		// Improvers
		Asset::add( 'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js', 'ie' );
		Asset::add( 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js', 'ie' );

		// App
		Asset::add( 'css/style.css' );

	}

}