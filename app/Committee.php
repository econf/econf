<?php

namespace App;

use App\Traits\TenantableTrait;
use Illuminate\Database\Eloquent\Model;

use Auth;
use Illuminate\Support\Str;

class Committee extends Model
{

	use TenantableTrait;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'trans_key',
		'conference_id',
	];

	/**
	 * @var \Illuminate\Database\Eloquent\Collection|static[]
	 */
	protected static $currentUserCommittees = null;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users(){
		return $this->belongsToMany('App\User')->orderBy('name')->withPivot('chair')->withTimestamps();
	}

	public function full_name(){
		if($this->trans_key){
			return trans($this->trans_key);
		}
		$name = $this->name . '_committee';
		$name = ucwords(str_replace('_', ' ', Str::snake($name)));
		return $name;
	}

	private static function populateCurrentCommittees() {
		$user = Auth::user();

		if ( is_null( self::$currentUserCommittees ) ) {
			self::$currentUserCommittees = $user->committees;
		}
	}

	/**
	 * Checks if the current user is in any of the given committees
	 *
	 * @param mixed $committees Committees.
	 *
	 * @return bool
	 */
	public static function in($committees, $chair = false) {
		if(!Auth::check()){
			return false;
		}
		
		self::populateCurrentCommittees();

		// Super admin
		if(self::inSingle('organizing')){
			return true;
		}

		if(is_array($committees)){
			foreach($committees as $committee){
				if(self::inSingle($committee, $chair)){
					return true;
				}
			}
			return false;
		} else {
			return self::inSingle($committees, $chair);
		}

	}

	private static function inSingle($committee, $chair = false){
		if(is_numeric($committee)){
			// ID
			$com = self::$currentUserCommittees->filter(function($value, $key) use ($committee){
				return $value->id == $committee;
			});
			if($com->isEmpty()){
				return false;
			}
			$com = $com->first();
		} else if ($committee instanceof Committee){
			// Object
			if(!self::$currentUserCommittees->contains($committee)){
				return false;
			}
			$com = $committee;
		} else {
			$com = self::$currentUserCommittees->filter(function($value, $key) use ($committee){
				return $value->name == $committee;
			});
			if($com->isEmpty()){
				return false;
			}
			$com = $com->first();
		}

		if($chair){
			return $com->pivot->chair;
		}

		return true;
	}

	public static function chair(){
		if(!Auth::check()){
			return false;
		}

		self::populateCurrentCommittees();

		// Super admin
		if(self::inSingle('organizing')){
			return true;
		}

		foreach (self::$currentUserCommittees as $committee){
			if($committee->pivot->chair){
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns the current user first committee or null otherwise.
	 *
	 * @return Committee The user current committee
	 */
	public static function current(){
		if(!Auth::check()){
			return null;
		}

		self::populateCurrentCommittees();

		return self::$currentUserCommittees->first();
	}

	public static function needs($committees) {
		if(!self::in($committees)){
			abort(503);
		}
	}

	public static function any(){
		if(!Auth::check()){
			return false;
		}

		self::populateCurrentCommittees();

		return ! self::$currentUserCommittees->isEmpty();
	}
}
