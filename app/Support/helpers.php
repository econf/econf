<?php

use App\Http\Middleware\LocaleMiddleware;

function m_action($url, $parameters = [], $absolute = true){
	if(config('econf.multi')) {
		if(is_array($parameters)){
			$parameters['confSlug'] = config( 'econf.conference.slug' );
		} else {
			$parameters = [config( 'econf.conference.slug' ), $parameters];
		}
	}
	return action($url, $parameters, $absolute);
}

function m_path($path){
	if(config('econf.multi')){
		return config('econf.conference.slug').'/'.$path;
	}
	return $path;
}

function setting($key, $conference = null, $default = null){

	$query = DB::table(config('settings.table'))->where('key', $key);

	if(is_null($conference)){
		$query->whereNull('conference_id');
	} else {
		$query->where('conference_id', $conference);
	}

	$setting = $query->value('value');

	if(empty($setting)){
		return $default;
	}

	return $setting;

}

function setting_input($label, $name, $type = 'text', $default = ''){

	return call_user_func('\BootForm::'.$type, $label, 'setting['.$name.']', old('setting.'.$name, Setting::get($name, $default)));

}

function scope_locale($locale, $func, $user = null){
	LocaleMiddleware::setLocale($locale);
	$func();
	LocaleMiddleware::setUserLocale($user);
}