<?php

namespace App;

use App\Traits\HasDataFieldTrait;
use App\Traits\TenantableTrait;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Mail;
use Setting;

class Invite extends Model {

	const TOKEN_LENGTH = 10;
	use TenantableTrait;
	use HasDataFieldTrait;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'email',
		'data',
	];

	protected $casts = [
		'data' => 'array',
	];

	/**
	 * @inheritDoc
	 */
	protected static function boot() {
		parent::boot();

		self::creating( function ( Invite $invite ) {

			$data = $invite->data;
			$data['inviter_id'] = Auth::id();

			$invite->data = $data;
			$invite->token = $invite->createNewToken();
		} );
	}

	/**
	 * Create a new token for the user.
	 *
	 * @return string
	 */
	protected function createNewToken() {
		$token = Str::random( self::TOKEN_LENGTH );
		$existing = Invite::where( 'token', $token )->count() > 0;
		if ( $existing ) {
			return self::createNewToken();
		}
		return $token;
	}
	
	public static function make( $email, $data, $trans_ns, $msg = "", \Closure $trans_data_func = null ) {

		$invite = Invite::create([
			'email' => $email,
			'data' => $data,
		]);

		$locale = config('app.locale');
		$user = User::whereEmail($email)->first();
		
		if(!empty($user)){
			$locale = $user->data('locale', $locale);
		}

		scope_locale($locale, function() use ($email, $msg, $trans_data_func, $trans_ns, $invite){

			$trans_data = [
				'conf_name' => Setting::get( 'conf-short_name', Setting::get( 'conf-name' ) ),
				'inviter' => Auth::user()->short_name
			];

			if(!is_null($trans_data_func)){
				$trans_data = array_merge($trans_data_func(), $trans_data);
			}

			$mail_data = compact('invite', 'trans_ns', 'trans_data', 'msg');

			Mail::send( 'invites.emails.invite', $mail_data, function ( $m ) use ( $email, $trans_data, $trans_ns ) {
				$m->to( $email )->subject( trans( $trans_ns.'.subject', $trans_data ) );
			} );
		});

	}

}
