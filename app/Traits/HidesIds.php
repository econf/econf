<?php
/**
 * Created by IntelliJ IDEA.
 * User: joaoluis
 * Date: 30/03/16
 * Time: 11:23
 */

namespace App\Traits;


use Hashids\Hashids;

trait HidesIds {

	protected static function hashids(){
		return new Hashids(config('app.key').get_called_class(), 4);
	}

	public function getHiddenIdAttribute(){
		return self::hashids()->encode($this->id);
	}

	public function getRouteKeyName(){
		return 'hiddenId';
	}

	public static function findByHiddenId($hid){
		return self::find(self::decodeHiddenId($hid));
	}

	public static function findByHiddenIdOrFail($hid){
		return self::findOrFail(self::decodeHiddenId($hid));
	}

	public static function decodeHiddenId($hid){
		$decoded = self::hashids()->decode($hid);
		if(empty($decoded)){
			return 0;
		}
		return $decoded[0];
	}

}