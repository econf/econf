<?php

namespace App\Traits;

use App;
use App\Scopes\TenantScope;

trait TenantableTrait {
	public static function bootTenantableTrait() {

		// Add Global scope that will handle all operations except create()
		$tenantScope = App::make( 'App\Scopes\TenantScope' );
		static::addGlobalScope( $tenantScope );

		// Now handle creating!
		static::creating( function ( $model ){
			if(empty($model->{TenantScope::$tenant_col})) {
				$model->{TenantScope::$tenant_col} = TenantScope::getTenantId();
			}
		} );

	}

	public static function allTenants() {
		return with( new static() )->newQueryWithoutScope( new TenantScope() );
	}

	public function getTenantWhereClause( $tenantColumn, $tenantId ) {
		if(is_null($tenantId)){
			return "{$this->getTable()}.{$tenantColumn} IS NULL";
		}
		return "{$this->getTable()}.{$tenantColumn} = {$tenantId}";
	}

}