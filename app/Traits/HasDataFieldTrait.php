<?php

namespace App\Traits;

use Illuminate\Support\Arr;

trait HasDataFieldTrait {

	public function data($key, $default = null){
		$data = Arr::get($this->data, $key, $default);
		if(is_array($data)){
			return collect($data);
		}
		return $data;
	}

}