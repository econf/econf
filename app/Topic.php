<?php

namespace App;

use App\Traits\TenantableTrait;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model {

	use TenantableTrait;

	protected $fillable = ['name'];

	/**
	 * Update the topics from the topics array passed.
	 *
	 * @param $topics string[] The array of topics
	 */
	public static function sync( $topics ) {

		array_walk($topics, function(&$val,$idx){ $val = trim($val); });

		$existing = self::all();

		foreach($existing as $et){
			if(!in_array($et->name, $topics)){
				$et->delete();
			} else {
				$topics = array_diff($topics, [$et->name]);
			}
		}

		foreach ( $topics as $topic ) {
			if(!empty($topic)){
				Topic::create(['name' => $topic]);
			}
		}

	}
}
