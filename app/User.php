<?php

namespace App;

use App\Traits\HasDataFieldTrait;
use App\Traits\TenantableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Gravatar;
use Illuminate\Support\Arr;

class User extends Authenticatable {

	use TenantableTrait;
	use HasDataFieldTrait;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	protected $casts = [
		'data' => 'array',
	];

	public function getPhotoAttribute() {
		return Gravatar::src( $this->email );
	}

	public function getShortNameAttribute() {
		$split = explode( ' ', $this->name );
		if ( count( $split ) < 2 ) {
			return $this->name;
		}
		return $split[0] . ' ' . $split[count( $split ) - 1];
	}

	public function committees() {
		return $this->belongsToMany( 'App\Committee' )->withPivot( 'chair' )->withTimestamps();
	}

}
