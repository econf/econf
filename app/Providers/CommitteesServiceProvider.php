<?php

namespace App\Providers;

use App\Committee;
use Auth;
use Blade;
use Eventy;
use Flash;
use Illuminate\Support\ServiceProvider;

class CommitteesServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
		Blade::directive( 'in', function ( $expression ) {
			return "<?php if (\\App\\Committee::in({$expression})) : ?>";
		} );

		Blade::directive( 'endin', function ( $expression ) {
			return "<?php endif; // App\\Committee::in ?>";
		} );

		Eventy::addFilter( 'invite.messages.committee', function ( $rules, $invite ) {

			$committee = Committee::find( $invite->data['committee'] );

			if ( !empty($committee) ) {
				$text = trans( 'econf.committee.invite_text', [ 'committee' => $committee->full_name() ] );
				if ( $invite->data( 'chair', false ) ) {
					$text = trans( 'econf.committee.invite_text_chair', [ 'committee' => $committee->full_name() ] );
				}

				$rules[] = [
					'title' => $committee->full_name(),
					'text' => $text
				];
			}

			return $rules;
		}, 20, 2 );

		Eventy::addAction( 'invite.accept.committee', function ( $invite ) {

			$committee = Committee::find( $invite->data['committee'] );

			if ( !empty($committee) ) {
				$committee->users()->attach( Auth::id(), [ 'chair' => $invite->data( 'chair', false ) ] );

				Flash::success( trans( 'econf.committee.invite_accepted', [ 'committee' => $committee->full_name() ] ) );
			}

		}, 20, 1 );
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
