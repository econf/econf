<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer(
            'network.layouts.admin', 'App\Http\ViewComposers\NetworkAdminComposer'
        );
        view()->composer(
            'layouts.admin', 'App\Http\ViewComposers\AdminComposer'
        );
        view()->composer(
            'layouts.public', 'App\Http\ViewComposers\PublicComposer'
        );
        view()->composer(
            'errors.*', 'App\Http\ViewComposers\ErrorComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
