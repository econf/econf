<?php

namespace App\Providers;

use App\Country;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		Validator::extend( 'hexcolor', function ( $attribute, $value, $parameters, $validator ) {
			$pattern = '/^#?[a-fA-F0-9]{3,6}$/';
			return (boolean) preg_match( $pattern, $value );
		} );

		Validator::extend( 'words', function ( $attribute, $value, $parameters, $validator ) {

			$validator->addReplacer('words', function($message, $attribute, $rule, $parameters){
				return str_replace(':count', $parameters[0], $message);
			});

			$words = preg_split( '@\s+@i', $value );
			if ( count( $words ) <= $parameters[0] ) {
				return true;
			}
			return false;
		} );

		Validator::extend( 'country', 'App\Country@validate' );
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		//
		$this->app->singleton( 'country', function ( $app ) {
			return new Country();
		} );
	}
}
