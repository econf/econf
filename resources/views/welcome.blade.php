<!DOCTYPE html>
<html>
<head>

    @include('network.layouts.common-header')

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 300;
            font-family: 'Lato';
            background-color: #f7f7f7;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }

        .logo {
            max-width: 80%;
            max-height: 150px;
            width: auto;
            height: auto;

        }

        .action{
            position: absolute;
            top: 30px;
            right: 30px;
            color: #AAA;
        }

        .btn{
            display: inline-block;
            background-color: #E6E6E6;
            color: #888;
            text-transform: uppercase;
            padding: .7em 1.2em;
            font-weight: 400;
            text-decoration: none;
            margin-left: 1em;
            -webkit-transition: background-color .3s;
            -moz-transition: background-color .3s;
            transition: background-color .3s;
        }

        .btn:hover{
            background-color: #ddd;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        {!! Html::image('images/logo.png', 'E-Conf', ['class' => 'logo']) !!}
    </div>
    <div class="action">
        @if(Auth::check())
            {{ Auth::user()->short_name }}
            <a href="{{ action('Network\DashboardController@view') }}" class="btn">
                Admin
            </a>
        @else
            <a href="{{ action('Network\Auth\AuthController@showLoginForm') }}" class="btn">
                Login
            </a>
        @endif
    </div>
</div>
</body>
</html>