@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            {{ trans('econf.dashboard.dashboard') }}
        </h1>
        {!! Breadcrumbs::render('admin.dashboard') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        @in('organizing')

        <div class="row">

            @foreach($data as $item)

                <div class="col-lg-3 col-sm-6">

                    <div class="info-box {{ !empty($item['class'])?$item['class']:'bg-aqua' }}">
                        <span class="info-box-icon"><i class="{{ $item['icon'] }}"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">{{ $item['label'] }}</span>
                            <span class="info-box-number">
                                {{ $item['value'] }}
                                @if(!empty($item['small']))
                                    <small>{{ $item['small'] }}</small>
                                @endif
                            </span>

                            @if(!empty($item['progress']) || !empty($item['footer']))
                                <div class="progress">
                                    <div class="progress-bar"
                                         style="width: {{ !empty($item['progress'])?$item['progress']:0 }}%"></div>
                                </div>
                                <span class="progress-description">
                                {{ !empty($item['footer'])?$item['footer']:(Setting::get('conf-short_name', Setting::get('conf-name'))) }}
                              </span>
                            @endif
                        </div>
                        <!-- /.info-box-content -->
                    </div>

                </div>

            @endforeach

        </div>

        @endin

        @action('dashboard.html')

        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title">
                            {{ trans('econf.admin.important_dates') }}
                        </h3>
                    </div>
                    <div class="box-body">
                        <ul class="list-group list-group-unbordered">
                            @foreach($calendar as $dates)
                                <li class="list-group-item">
                                    <strong>{{ $dates['label'] }}</strong><br/>
                                    {{ $dates['start']->format(trans('econf.date.longDate')) }}
                                    {{ trans('econf.public.to') }}
                                    {{ $dates['end']->format(trans('econf.date.longDate')) }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection