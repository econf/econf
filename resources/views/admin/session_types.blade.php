@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            {{ trans('econf.session_types.label') }}
        </h1>
        {!! Breadcrumbs::render('admin.settings.session_types') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <p>
            <a class="btn btn-default" title="{{ trans('econf.actions.create') }}"
               data-toggle="modal" data-target="#createType">
                <span class="fa fa-plus"></span> {{ trans('econf.actions.create') }}
            </a>
        </p>


        <div class="box box-default">
            <div class="box-body no-padding">
                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ trans('econf.session_types.name') }}</th>
                        <th>{{ trans('econf.session_types.duration_min') }}</th>
                        <th>{{ trans('econf.session_types.parallel') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(\App\SessionType::all() as $session_type)
                        <tr>
                            <td>{{ $session_type->name }}</td>
                            <td>{{ $session_type->duration }}</td>
                            <td>
                                @if($session_type->parallel)
                                    <span class="fa fa-check text-success"></span>
                                @else
                                    <span class="fa fa-times text-muted"></span>
                                @endif
                            </td>
                            <td class="text-right">
                                <div class="btn-group btn-group-xs">
                                    <a class="btn btn-default" title="{{ trans('econf.actions.edit') }}"
                                       data-toggle="modal" data-target="#edit{{ $session_type->id }}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a class="btn btn-danger" title="{{ trans('econf.actions.delete') }}"
                                       data-toggle="modal" data-target="#delete{{ $session_type->id }}">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section>
    <!-- /.content -->

    <div class="modal modal-default" id="createType">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">{{ trans('econf.session_types.create') }}</h4>
                </div>
                {!! BootForm::open()->action(m_action('Admin\SessionTypeController@store')) !!}
                <div class="modal-body">
                    {!! BootForm::text(trans('econf.session_types.name'), 'name') !!}
                    {!! BootForm::text(trans('econf.session_types.duration_min'), 'duration')->attribute('type', 'number')->attribute('min', 0)->attribute('step', 1) !!}
                    {!! BootForm::checkbox(trans('econf.session_types.parallel'), 'parallel') !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        {{ trans('econf.actions.cancel') }}
                    </button>
                    {!! BootForm::submit(trans('econf.actions.create'), 'btn-primary') !!}
                </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>

    @foreach(\App\SessionType::all() as $session_type)

        <div class="modal modal-default" id="edit{{ $session_type->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">{{ trans('econf.session_types.edit') }}</h4>
                    </div>
                    {!! BootForm::open()->action(m_action('Admin\SessionTypeController@update', $session_type))->put() !!}
                    {!! BootForm::bind($session_type) !!}
                    <div class="modal-body">
                        {!! BootForm::text(trans('econf.session_types.name'), 'name') !!}
                        {!! BootForm::text(trans('econf.session_types.duration_min'), 'duration')->attribute('type', 'number')->attribute('min', 0)->attribute('step', 1) !!}
                        {!! BootForm::checkbox(trans('econf.session_types.parallel'), 'parallel') !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            {{ trans('econf.actions.cancel') }}
                        </button>
                        {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}
                    </div>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>

        <div class="modal modal-danger" id="delete{{ $session_type->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">{{ trans('econf.session_types.delete') }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>{!! trans('econf.session_types.delete_confirmation', ['name' => $session_type->name]) !!}</p>
                        <p>{!! trans('econf.layout.delete_warning') !!}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                            {{ trans('econf.actions.cancel') }}
                        </button>
                        {!! BootForm::open()->action(m_action('Admin\SessionTypeController@destroy',$session_type))->delete() !!}
                        {!! BootForm::submit(trans('econf.actions.delete'), 'btn-outline') !!}
                        {!! BootForm::close() !!}
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
@endsection