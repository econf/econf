@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            {{ trans('econf.admin.settings.conference.label') }}
        </h1>
        {!! Breadcrumbs::render('admin.settings.conference') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('Admin\SettingsController@store'))->post()->multipart() !!}

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.admin.settings.conference.data') }}
                </h3>
            </div>
            <div class="box-body">

                {!! setting_input(trans('econf.admin.settings.conference.name'), 'conf-name') !!}
                {!! setting_input(trans('econf.admin.settings.conference.short_name'), 'conf-short_name') !!}
                {!! setting_input(trans('econf.admin.settings.conference.email'), 'conf-email', 'email') !!}
                {!! setting_input(trans('econf.admin.settings.conference.start_date'), 'conf-start_date', 'date') !!}
                {!! setting_input(trans('econf.admin.settings.conference.end_date'), 'conf-end_date', 'date') !!}

                {!! BootForm::textarea(trans('econf.admin.settings.conference.topics'), 'topics')->value(old('topics', $topics))->rows(2)->helpBlock(trans('econf.admin.settings.conference.topics_one-per-line')) !!}

            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.admin.settings.conference.brand') }}
                </h3>
            </div>
            <div class="box-body">

                @if(Setting::has('conf-logo'))
                    <div class="settings-logo pull-right img-thumbnail" style="margin-left:10px;">
                        <img src="{{ action('FileController@get', Setting::get('conf-logo')) }}">
                    </div>
                @endif
                {!! BootForm::file(trans('econf.admin.settings.conference.logo'), 'logo')->helpBlock(trans('econf.layout.file_formats', ['types' => 'PNG']))->attribute('accept', 'image/png') !!}

                @if(Setting::has('conf-logo_white'))
                    <div class="settings-logo white pull-right img-thumbnail" style="margin-left:10px;">
                        <img src="{{ action('FileController@get', Setting::get('conf-logo_white')) }}">
                    </div>
                @endif
                {!! BootForm::file(trans('econf.admin.settings.conference.logo_white'), 'logo_white')->helpBlock(trans('econf.layout.file_formats', ['types' => 'PNG']))->attribute('accept', 'image/png') !!}

                @if(Setting::has('conf-icon'))
                    <div class="settings-icon pull-right img-thumbnail" style="margin-left:10px;">
                        <img src="{{ action('FileController@get', Setting::get('conf-icon')) }}" class="img-rounded">
                    </div>
                @endif
                {!! BootForm::file(trans('econf.admin.settings.conference.icon'), 'icon')->helpBlock(trans('econf.admin.settings.conference.icon_help').' '.trans('econf.layout.file_formats', ['types' => 'PNG']))->attribute('accept', 'image/png') !!}

                <div class="form-group {!! $errors->has('setting[conf-color]') ? 'has-error' : '' !!}">
                    <label for="setting[school.color]">{{trans('econf.admin.settings.conference.color')}}</label>
                    <div class="input-group colorinput">
                        <span class="input-group-addon"><i></i></span>
                        <input type="text" class="form-control" name="setting[conf-color]" id="setting[conf-color]"
                               value="{{Setting::get('conf-color', '')}}">
                    </div>
                    {!! $errors->first('setting[conf-color]', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>
    <!-- /.content -->
@endsection