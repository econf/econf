@extends('layouts.admin')

@section('content')
    <header class="content-header">
        <h1>{{ $committee->full_name() }}</h1>

        {!! Breadcrumbs::render('committee', $committee) !!}
    </header>

    <section class="content">

        @include('flash::message')

        <p>
            <a class="btn btn-default" data-toggle="modal" data-target="#addUserModal">
                <span class="fa fa-user-plus"></span>
                {{ trans('econf.committee.invite_user') }}
            </a>
        </p>

        <div class="box box-default">
            <div class="box-body no-padding">

                <table class="table">
                    <thead>
                    <tr>
                        <th style="width: 1.8em;"></th>
                        <th>{{ trans('econf.user.name') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <img src="{{ $user->photo }}" alt="{{ $user->short_name }}" class="img-circle"
                                     style="height: 1.8em; width: auto; margin: -.4em 0;">
                            </td>
                            <td>
                                {{ $user->name }}
                                @if($user->pivot->chair)
                                    <span class="label label-default">{{ trans('econf.committee.chair') }}</span>
                                @endif
                            </td>
                            <td class="text-right">
                                @if($user->id != Auth::id())
                                    @if($user->pivot->chair)
                                        {!! BootForm::open()->action(m_action('\\'.$class.'@removeChair', $user->id))->delete() !!}
                                    @else
                                        {!! BootForm::open()->action(m_action('\\'.$class.'@makeChair', $user->id))->put() !!}
                                    @endif
                                    <div class="btn-group btn-group-xs">
                                        @if($user->pivot->chair)
                                            <button class="btn btn-default btn-xs" type="submit"
                                                    title="{{ trans('econf.committee.remove_chair') }}">
                                                <span class="fa fa-star-o"></span>
                                            </button>
                                        @else
                                            <button class="btn btn-default btn-xs" type="submit"
                                                    title="{{ trans('econf.committee.make_chair') }}">
                                                <span class="fa fa-star"></span>
                                            </button>

                                        @endif
                                        <a href="mailto:{{ $user->email }}"
                                           class="btn btn-default" title="{{ trans('econf.actions.send_email') }}">
                                            <span class="fa fa-envelope"></span>
                                        </a>
                                        <a href="#" class="btn btn-danger"
                                           title="{{ trans('econf.committee.remove_user') }}"
                                           data-toggle="modal" data-target="#deleteModal{{ $user->id }}">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </div>
                                    {!! BootForm::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

            <div class="box-footer">
            <span class="text-muted">
                {{ trans('econf.layout.results', ['from' => $users->firstItem(), 'to' => $users->lastItem(), 'total' => $users->total()]) }}
            </span>
                <div class="box-tools pull-right">
                    {!! $users->links() !!}
                </div>
            </div>
        </div>


    </section>

    <!-- Add user modal -->

    <div class="modal fade" id="addUserModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">
                        {{ trans('econf.committee.invite_user') }}
                    </h4>
                </div>
                {!! BootForm::open()->action(m_action('\\'.$class.'@addUser'))->put() !!}
                <div class="modal-body">
                    {!! BootForm::textarea(trans('econf.user.email'), 'email')->rows(2)->helpBlock(trans('econf.committee.one_per_line')) !!}
                    {!! BootForm::checkbox(trans('econf.committee.chair'), 'chair') !!}
                    {!! BootForm::textarea(trans('econf.admin.email.message'), 'message')->addClass('koala-simple') !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    {!! BootForm::submit(trans('econf.actions.invite'), 'btn-primary') !!}
                </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>

    @foreach($users as $user)
        @if($user->id != Auth::id())
            <div class="modal modal-danger fade" id="deleteModal{{ $user->id }}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">
                                {{ trans('econf.committee.remove_user') }}
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>{!! trans('econf.committee.remove_confirmation', ['name' => $user->name, 'committee' => $committee->full_name()]) !!}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                                {{ trans('econf.actions.cancel') }}
                            </button>
                            {!! BootForm::open()->action(m_action('\\'.$class.'@removeUser', $user))->delete() !!}
                            {!! BootForm::submit(trans('econf.actions.remove'), 'btn-outline') !!}
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach

@endsection