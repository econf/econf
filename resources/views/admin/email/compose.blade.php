@extends('layouts.admin')

@section('content')
    <header class="content-header">
        <h1>{{ trans('econf.admin.email.compose') }}</h1>

        {!! Breadcrumbs::render('admin.email') !!}
    </header>

    <section class="content">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('Admin\EmailController@send')) !!}

        {!! BootForm::hidden('group')->value($group['id']) !!}


            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('econf.admin.email.message_details') }}</h3>
                </div>
                <div class="box-body">
                    <p>
                        <strong>{{ trans('econf.admin.email.group') }}:</strong>
                        {{ $group['name'] }}
                    </p>
                    @if(Request::has('individual'))
                        {!! BootForm::select(trans('econf.admin.email.recipients'), 'recipients', $members->pluck('toString', 'id'))->multiple()->addClass( 'select2' )->style( 'width:100%;' ) !!}
                    @endif
                    {!! BootForm::text(trans('econf.admin.email.subject'), 'subject')->value($template['subject']) !!}
                </div>
            </div>

        <div class="row">
            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('econf.admin.email.message') }}</h3>
                    </div>
                    <div class="box-body no-padding koala-box medium">
                        {!! BootForm::textarea(trans('econf.admin.email.message'), 'message')->hideLabel()->value($template['body']) !!}
                    </div>
                    <div class="box-footer">
                        {!! BootForm::submit(trans('econf.actions.send_email'), 'btn-primary') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('econf.admin.email.variables') }}</h3>
                    </div>
                    <div class="box-body">
                        @foreach($vars as $var => $text)
                            <div class="row">
                                    <span class="col-xs-4" style="font-weight: bold;">
                                        [:{{ $var }}:]
                                    </span>
                                    <span class="col-xs-8">
                                        {{ $text }}
                                    </span>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        {!! BootForm::close() !!}

    </section>
@endsection