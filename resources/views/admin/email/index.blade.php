@extends('layouts.admin')

@section('content')
    <header class="content-header">
        <h1>{{ trans('econf.admin.email.label') }}</h1>

        {!! Breadcrumbs::render('admin.email') !!}
    </header>

    <section class="content">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('Admin\EmailController@compose'))->get() !!}

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('econf.admin.email.pick_recipient') }}</h3>
            </div>
            <div class="box-body">
                {!! BootForm::select(trans('econf.admin.email.group'), 'group', $groups->pluck('name', 'id'))->addClass( 'select2' )->style( 'width:100%;' ) !!}
                {!! BootForm::checkbox(trans('econf.admin.email.select_individual'), 'individual') !!}
                {!! BootForm::select(trans('econf.admin.email.template'), 'template', $templates->pluck('name', 'id'))->addClass( 'select2' )->style( 'width:100%;' ) !!}
            </div>
            <div class="box-footer">
                {!! BootForm::submit(trans('pagination.next'), 'btn-primary') !!}
            </div>
        </div>

        {!! BootForm::close() !!}

    </section>
@endsection