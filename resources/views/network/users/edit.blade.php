@extends('network.layouts.admin')

@section('content')

    <header class="content-header">
        <h1>
            {{ trans('econf.network.edit_user') }}
        </h1>

        {!! Breadcrumbs::render('network.users.edit') !!}
    </header>

    <section class="content">

        {!! BootForm::open()->action(action('Network\UserController@update', $user))->put() !!}
        {!! BootForm::bind($user) !!}

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.network.user_data') }}
                </h3>
            </div>
            <div class="box-body">
                {!! BootForm::text(trans('econf.user.name'), 'name') !!}
                {!! BootForm::email(trans('econf.user.email'), 'email') !!}
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}
        <a href="{{ action('Network\UserController@index') }}" class="btn btn-default">
            {{ trans('econf.actions.cancel') }}
        </a>

        <div class="pull-right">
            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                {{ trans('econf.actions.delete') }}
            </a>
        </div>

        {!! BootForm::close() !!}

    </section>

    <div class="modal modal-danger" id="deleteModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">{{ trans('econf.network.delete_user') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{!! trans('econf.network.delete_user_confirmation', ['name' => $user->short_name]) !!}</p>
                    <p>{!! trans('econf.network.delete_warning') !!}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                        {{ trans('econf.actions.cancel') }}
                    </button>
                    {!! BootForm::open()->action(action('Network\UserController@destroy', $user))->delete() !!}
                    {!! BootForm::submit(trans('econf.actions.delete'), 'btn-outline') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection