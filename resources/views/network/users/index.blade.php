@extends('network.layouts.admin')

@section('content')
    <header class="content-header">
        <h1>{{ trans('econf.network.users') }}</h1>

        {!! Breadcrumbs::render('network.users') !!}
    </header>

    <section class="content">

        @include('flash::message')

        <p>
            <a href="{{ action('Network\UserController@create') }}" class="btn btn-default">
                <span class="fa fa-plus"></span>
                {{ trans('econf.network.new_user') }}
            </a>
        </p>

        <div class="box box-default">
            <div class="box-body no-padding">

                <table class="table">
                    <thead>
                    <tr>
                        <th style="width: 1.8em;"></th>
                        <th>{{ trans('econf.user.name') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <img src="{{ $user->photo }}" alt="{{ $user->short_name }}" class="img-circle" style="height: 1.8em; width: auto; margin: -.4em 0;">
                            </td>
                            <td>{{ $user->name }}</td>
                            <td class="text-right">
                                <div class="btn-group btn-group-xs">
                                    <a href="mailto:{{ $user->email }}"
                                       class="btn btn-default" title="{{ trans('econf.actions.send_email') }}">
                                        <span class="fa fa-envelope"></span>
                                    </a>
                                    <a href="{{ action('Network\UserController@edit', $user) }}"
                                       class="btn btn-default" title="{{ trans('econf.actions.edit') }}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

            <div class="box-footer">
            <span class="text-muted">
                {{ trans('econf.layout.results', ['from' => $users->firstItem(), 'to' => $users->lastItem(), 'total' => $users->total()]) }}
            </span>
                <div class="box-tools pull-right">
                    {!! $users->links() !!}
                </div>
            </div>
        </div>


    </section>

@endsection