@extends('network.layouts.admin')

@section('content')

    <header class="content-header">
        <h1>{{ trans('econf.network.new_user') }}</h1>

        {!! Breadcrumbs::render('network.users.new') !!}
    </header>

    <section class="content">

        {!! BootForm::open()->action(action('Network\UserController@store'))->post() !!}

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.network.user_data') }}
                </h3>
            </div>
            <div class="box-body">
                {!! BootForm::text(trans('econf.user.name'), 'name') !!}
                {!! BootForm::email(trans('econf.user.email'), 'email')->helpBlock(trans('econf.user.password_to_this_email')) !!}
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.create'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>

@endsection