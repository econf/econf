@extends('network.layouts.email')

@section('content')

    <h2 class="first">{{ trans('econf.network.new_user_email.subject') }}</h2>

    <p>{{ trans('econf.email.dear_name', ['name' => $user_name]) }}</p>

    <p>{{ trans('econf.network.new_user_email.top') }}</p>

    <p>{{ trans('econf.network.new_user_email.credentials') }}</p>

    <p>
        <strong>{{trans('econf.user.email')}}:</strong> {{ $user_email }}<br />
        <strong>{{trans('econf.user.password')}}:</strong> {{ $password }}
    </p>

    <p>{{ trans('econf.network.new_user_email.bottom', ['link' => $url]) }}</p>

    <!-- button -->
    <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <a href="{{ $url }}">{{trans('econf.actions.go')}}</a>
            </td>
        </tr>
    </table>
    <!-- /button -->


@endsection