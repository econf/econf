@extends('network.layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            {{ trans('econf.dashboard.dashboard') }}
        </h1>
        {!! Breadcrumbs::render('network.dashboard') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $conf_count }}</h3>

                        <p>{{ trans('econf.dashboard.conferences') }}</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        {{ trans('econf.actions.view') }} <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-maroon">
                    <div class="inner">
                        <h3>{{ $user_count }}</h3>

                        <p>{{ trans('econf.dashboard.users') }}</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-group"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        {{ trans('econf.actions.view') }} <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection