@extends('network.layouts.box')

@section('content')

    {!! BootForm::open()->action(action('Network\Auth\AuthController@login'))->post() !!}
    {!! BootForm::email(trans('econf.user.email'), 'email') !!}
    {!! BootForm::password(trans('econf.user.password'), 'password') !!}

    <div class="row">
        <div class="col-xs-8">
            <div class="checkbox icheck">
                <label>
                    <input type="checkbox" name="remember"> Remember Me
                </label>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            {!! BootForm::submit(trans('econf.auth.login'), 'btn-primary btn-block btn-flat') !!}
        </div>
        <!-- /.col -->
    </div>

    {!! BootForm::close() !!}

    <div class="text-center">
        <a href="{{ action('Network\Auth\PasswordController@showResetForm') }}">
            {{trans('econf.auth.forgot_password')}}
        </a>
    </div>

@endsection