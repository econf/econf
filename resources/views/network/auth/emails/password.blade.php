@extends('network.layouts.email')

@section('content')

    <h2 class="first">{{ trans('econf.auth.reset_password_mail.subject') }}</h2>

    <p>{{ trans('econf.auth.reset_password_mail.click_below') }}</p>


    <!-- button -->
    <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">{{trans('econf.auth.reset')}}</a>
            </td>
        </tr>
    </table>
    <!-- /button -->


@endsection