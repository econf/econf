@extends('network.layouts.box')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <h3>{{trans('econf.auth.reset_password')}}</h3>

{!! BootForm::open()->action(m_action('Network\Auth\PasswordController@sendResetLinkEmail'))->post() !!}
{!! BootForm::email(trans('econf.user.email'), 'email')->value(old('email')) !!}

<div class="row">
    <div class="col-xs-4 col-xs-offset-8">
        <button type="submit" class="btn btn-primary btn-block btn-flat">
            {{ trans('econf.auth.send_link') }}
        </button>
    </div>
    <!-- /.col -->
</div>

{!! BootForm::close() !!}

@endsection