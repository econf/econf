@extends('network.layouts.box')

@section('content')

    {!! BootForm::open()->action(m_action('Network\Auth\PasswordController@reset'))->post() !!}
    {!! BootForm::hidden('token')->value($token) !!}
    {!! BootForm::email(trans('econf.user.email'), 'email')->value($email or old('email')) !!}
    {!! BootForm::password(trans('econf.user.password'), 'password') !!}
    {!! BootForm::password(trans('econf.user.confirm_password'), 'password_confirmation') !!}
    <div class="row">
        <div class="col-xs-4 col-xs-offset-8">
            <button type="submit" class="btn btn-primary btn-block btn-flat">
                {{ trans('econf.auth.reset') }}
            </button>
        </div>
        <!-- /.col -->
    </div>
    {!! BootForm::close() !!}

@endsection