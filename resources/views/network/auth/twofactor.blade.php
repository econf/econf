@extends('network.layouts.box')

@section('content')

    {!! BootForm::open()->action(action('Network\Auth\AuthController@twoFactor'))->post() !!}

    <p class="text-center">
        <strong>{{ $user->short_name }}</strong>
    </p>

    {!! BootForm::text(trans('econf.user.2fa.code'), 'code') !!}

    <div class="row">
        <!-- /.col -->
        <div class="col-xs-4 col-xs-push-8">
            {!! BootForm::submit(trans('econf.auth.login'), 'btn-primary btn-block btn-flat') !!}
        </div>
        <!-- /.col -->
    </div>

    {!! BootForm::close() !!}


@endsection