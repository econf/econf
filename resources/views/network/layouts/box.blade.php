<!DOCTYPE html>
<html>
<head>

    @include('network.layouts.common-header')

    {{ Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
    {{ Html::style('bower_components/components-font-awesome/css/font-awesome.min.css') }}

    {{ Html::style('components/admin-lte/dist/css/AdminLTE.min.css') }}
    {{ Html::style('bower_components/iCheck/skins/square/blue.css') }}

    {{ Html::style('css/style.css') }}

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/') }}">
            {!! Html::image('images/logo.png', 'E-Conf') !!}
        </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">

        @yield('content')

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


{{ Html::script('bower_components/jquery/dist/jquery.min.js') }}
{{ Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') }}
{{ Html::script('bower_components/iCheck/icheck.min.js') }}

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>