<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>

    @include('network.layouts.common-header')

    {{ Asset::css() }}
    {{ Asset::styles() }}

    <!--[if lt IE 9]>
    {{ Asset::js('ie') }}
    <![endif]-->
</head>

<body class="hold-transition skin-blue fixed">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="{{ action('Network\DashboardController@view') }}" class="logo econf">
            {!! Html::image('images/logo-white.png') !!}
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            {!! Html::image(Auth::user()->photo, Auth::user()->name, ['class' => 'user-image']) !!}
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->short_name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                {!! Html::image(Auth::user()->photo, Auth::user()->name, ['class' => 'img-circle']) !!}

                                <p>
                                    {{ Auth::user()->name }}
                                    <small>{{ trans('econf.network.admin') }}</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">{{ trans('econf.user.profile') }}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ action('Network\Auth\AuthController@logout') }}" class="btn btn-default btn-flat">{{ trans('econf.auth.logout') }}</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    {!! Html::image(Auth::user()->photo, Auth::user()->name, ['class' => 'img-circle']) !!}
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->short_name }}</p>
                    {{ trans('econf.network.admin') }}
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                @include('partials.menu-items', array('items' => $NetworkMenu->roots()))
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @yield('content')

    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            <img src="{{ asset('images/logo.png') }}" alt="Powered by E-Conf" style="height: 18px;">
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{ date('Y') }} <a href="http://www.joaopluis.pt">João Luís</a>.</strong> {{ trans('econf.layout.all_rights_reserved') }}
    </footer>

</div>
<!-- ./wrapper -->

{{ Asset::js() }}
{{ Asset::scripts('footer') }}
{{ Asset::scripts('ready') }}

</body>
</html>