@extends('network.layouts.admin')

@section('content')

    <header class="content-header">
        <h1>
            {{ trans('econf.user.security') }}
        </h1>

        {!! Breadcrumbs::render('network.account.security') !!}
    </header>

    <section class="content">

        @include('flash::message')

        <div class="box box-default">
            {!! BootForm::open()->action(action('Network\AccountController@postSecurity'))->post() !!}
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.user.change_password') }}
                </h3>
            </div>
            <div class="box-body">
                {!! BootForm::password(trans('econf.user.current_password'), 'current_password') !!}
                {!! BootForm::password(trans('econf.user.new_password'), 'new_password') !!}
                {!! BootForm::password(trans('econf.user.confirm_password'), 'new_password_confirmation') !!}
            </div>
            <div class="box-footer">
                {!! BootForm::submit(trans('econf.actions.change'), 'btn-primary') !!}
            </div>
            {!! BootForm::close() !!}
        </div>

        <div class="box @if(Auth::user()->g2fa_key) box-success @else box-danger @endif">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.user.2fa.title') }}
                </h3>
            </div>
            <div class="box-body">
                <p>{{ trans('econf.user.2fa.description') }}</p>
                <p>{{ trans('econf.user.2fa.implementation', ['name' => 'E-Conf']) }}</p>
                @if(Auth::user()->g2fa_key)
                    <p class="text-success">{!! trans('econf.user.2fa.enabled') !!}</p>
                @else
                    <p class="text-danger">{!! trans('econf.user.2fa.disabled') !!}</p>
                @endif
            </div>
            <div class="box-footer">
                @if(Auth::user()->g2fa_key)
                    <a class="btn btn-danger" data-toggle="modal" data-target="#twoFactorModal">
                        {{ trans('econf.user.2fa.disable') }}
                    </a>
                @else
                    <a class="btn btn-success" data-toggle="modal" data-target="#twoFactorModal">
                        {{ trans('econf.user.2fa.enable') }}
                    </a>
                @endif
            </div>
        </div>

    </section>

    @if(Auth::user()->g2fa_key)
        {{-- Disable modal --}}
        <div class="modal modal-danger" id="twoFactorModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">{{ trans('econf.user.2fa.disable') }}</h4>
                    </div>
                    {!! BootForm::open()->action(action('Network\AccountController@deleteTwoFactor'))->delete() !!}
                    <div class="modal-body">

                        <p>{{ trans('econf.user.2fa.disable_instructions') }}</p>
                        {!! BootForm::text(trans('econf.user.2fa.code'), 'code') !!}

                    </div>
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-outline pull-left"
                                data-dismiss="modal">{{ trans('econf.actions.cancel') }}</button>
                        {!! BootForm::submit(trans('econf.user.2fa.disable'), 'btn-outline') !!}
                    </div>
                    {!! BootForm::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @else
        <div class="modal" id="twoFactorModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">{{ trans('econf.user.2fa.enable') }}</h4>
                    </div>
                    {!! BootForm::open()->action(action('Network\AccountController@postTwoFactor'))->post() !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-5 col-sm-push-7 text-center text-sm-right">
                                {{ Html::image($g2fa_url) }}
                            </div>
                            <div class="col-sm-7 col-sm-pull-5">
                                <p>{{ trans('econf.user.2fa.instructions') }}</p>
                                {!! BootForm::text(trans('econf.user.2fa.code'), 'code') !!}
                                {!! BootForm::hidden('token')->value($token) !!}
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-default pull-left"
                                data-dismiss="modal">{{ trans('econf.actions.cancel') }}</button>
                        {!! BootForm::submit(trans('econf.user.2fa.enable'), 'btn-primary') !!}
                    </div>
                    {!! BootForm::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endif

@endsection