@extends('network.layouts.admin')

@section('content')

    <header class="content-header">
        <h1>
            {{ trans('econf.user.profile') }}
        </h1>

        {!! Breadcrumbs::render('network.account.profile') !!}
    </header>

    <section class="content">

        @include('flash::message')

        {!! BootForm::open()->action(action('Network\AccountController@postProfile'))->post() !!}

        <div class="box box-default">
            <div class="box-body">
                {!! BootForm::text(trans('econf.user.name'), 'name')->value(Auth::user()->name) !!}
                {!! BootForm::email(trans('econf.user.email'), 'email')->value(Auth::user()->email) !!}
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>


@endsection