@extends('network.layouts.admin')

@section('content')

    <header class="content-header">
        <h1>{{ setting('conf-short_name', $conference->id, setting('conf-name', $conference->id)) }}</h1>
        {!! Breadcrumbs::render('network.conferences.show', $conference) !!}
    </header>

    <section class="content">

        <div class="row">
            <div class="col-md-6">
                @if(!empty(setting('conf-logo', $conference->id)))
                    <div class="box box-solid">
                        <div class="box-body text-center">
                            <img src="{{ action('FileController@get', setting('conf-logo', $conference->id)) }}"
                                 alt="{{ setting('conf-name', $conference->id) }}"
                                 style="max-width: 70%;max-height: 12em;padding: 1em 0;">
                        </div>
                    </div>
                @endif

                <div class="box box-default"
                     @if(!empty(setting('conf-color', $conference->id))) style="border-top-color: {{ setting('conf-color', $conference->id) }};" @endif >
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            {{ trans('econf.network.conference_data') }}
                        </h3>
                    </div>
                    <div class="box-body">
                        <p>
                            <strong>{{ trans('econf.network.slug') }}</strong><br>
                            {{ $conference->slug }}
                        </p>
                        <p>
                            <strong>{{ trans('econf.network.name') }}</strong><br>
                            {{ setting('conf-name', $conference->id) }}
                        </p>
                    </div>
                    <div class="box-footer">
                        <a href="{{ action('Network\ConferenceController@edit', $conference) }}"
                           class="btn btn-default">
                            <span class="fa fa-pencil"></span>
                            {{ trans('econf.actions.edit') }}
                        </a>
                        <div class="pull-right">
                            <a href="{{ action('PublicController@index', $conference->slug) }}" class="btn btn-default">
                                {{ trans('econf.network.visit_site') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            {{ trans('econf.network.conference_organizing_committee') }}
                        </h3>
                    </div>
                    <div class="box-body people-list">
                        @foreach($users as $user)
                            <div class="user-block">
                                {!! Html::image($user->photo, $user->shortName, ['class' => 'img-circle']) !!}
                                <span class="username">{{ $user->name }}</span>
                                <span class="description">
                                    <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                </span>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection