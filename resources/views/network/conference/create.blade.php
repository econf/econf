@extends('network.layouts.admin')

@section('content')

    <header class="content-header">
        <h1>{{ trans('econf.network.new_conference') }}</h1>

        {!! Breadcrumbs::render('network.conferences.new') !!}
    </header>

    <section class="content">

        {!! BootForm::open()->action(action('Network\ConferenceController@store'))->post() !!}

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.network.conference_data') }}
                </h3>
            </div>
            <div class="box-body">
                {!! BootForm::text(trans('econf.network.slug'), 'slug') !!}
                {!! BootForm::text(trans('econf.network.name'), 'name') !!}
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.network.conference_chair_data') }}
                </h3>
            </div>
            <div class="box-body">
                {!! BootForm::text(trans('econf.user.name'), 'user_name') !!}
                {!! BootForm::email(trans('econf.user.email'), 'user_email')->helpBlock(trans('econf.user.password_to_this_email')) !!}
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.create'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>

@endsection