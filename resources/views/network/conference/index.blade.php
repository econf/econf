@extends('network.layouts.admin')

@section('content')
    <header class="content-header">
        <h1>{{ trans('econf.network.conferences') }}</h1>

        {!! Breadcrumbs::render('network.conferences') !!}
    </header>

    <section class="content">

        @include('flash::message')

        <p>
            <a href="{{ action('Network\ConferenceController@create') }}" class="btn btn-default">
                <span class="fa fa-plus"></span>
                {{ trans('econf.network.new_conference') }}
            </a>
        </p>

        <div class="box box-default">
            <div class="box-body no-padding">

                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ trans('econf.network.slug') }}</th>
                        <th>{{ trans('econf.network.name') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($conferences as $conference)
                        <tr>
                            <td>{{ $conference->slug }}</td>
                            <td>{{ setting('conf-short_name', $conference->id, setting('conf-name', $conference->id)) }}</td>
                            <td class="text-right">
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ action('Network\ConferenceController@show', $conference) }}"
                                       class="btn btn-default" title="{{ trans('econf.actions.view') }}">
                                        <span class="fa fa-eye"></span>
                                    </a>
                                    <a href="{{ action('Network\ConferenceController@edit', $conference) }}"
                                       class="btn btn-default" title="{{ trans('econf.actions.edit') }}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

            <div class="box-footer">
            <span class="text-muted">
                {{ trans('econf.layout.results', ['from' => $conferences->firstItem(), 'to' => $conferences->lastItem(), 'total' => $conferences->total()]) }}
            </span>
                <div class="box-tools pull-right">
                    {!! $conferences->links() !!}
                </div>
            </div>
        </div>


    </section>

@endsection