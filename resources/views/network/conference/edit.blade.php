@extends('network.layouts.admin')

@section('content')

    <header class="content-header">
        <h1>
            {{ trans('econf.network.edit_conference') }}
            <small>{{ setting('conf.name', $conference->id) }}</small>
        </h1>

        {!! Breadcrumbs::render('network.conferences.edit', $conference) !!}
    </header>

    <section class="content">

        {!! BootForm::open()->action(action('Network\ConferenceController@update', $conference))->put() !!}
        {!! BootForm::bind($conference) !!}

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('econf.network.conference_data') }}
                </h3>
            </div>
            <div class="box-body">
                {!! BootForm::text(trans('econf.network.slug'), 'slug') !!}
            </div>
        </div>

        {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}
        <a href="{{ action('Network\ConferenceController@show', $conference) }}" class="btn btn-default">
            {{ trans('econf.actions.cancel') }}
        </a>

        <div class="pull-right">
            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                {{ trans('econf.actions.delete') }}
            </a>
        </div>

        {!! BootForm::close() !!}

    </section>

    <div class="modal modal-danger" id="deleteModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">{{ trans('econf.network.delete_conference') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{!! trans('econf.network.delete_conference_confirmation', ['name' => setting('conf.name', $conference->id)]) !!}</p>
                    <p>{!! trans('econf.network.delete_conference_warning') !!}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                        {{ trans('econf.actions.cancel') }}
                    </button>
                    {!! BootForm::open()->action(action('Network\ConferenceController@destroy', $conference))->delete() !!}
                    {!! BootForm::submit(trans('econf.actions.delete'), 'btn-outline') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection