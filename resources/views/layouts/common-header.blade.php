<meta charset="utf-8">
<title>
    {{ Setting::get('conf-short_name', Setting::get('conf-name')) }}
</title>

<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<link rel="manifest" href="{{ m_action('ManifestController@get') }}">

<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">

@if(Setting::get('conf-icon'))
    <link rel="icon" href="{{ action('FileController@get', Setting::get('conf-icon')) }}">
    <link rel="apple-touch-icon" href="{{ action('FileController@get', Setting::get('conf-icon')) }}">
@endif

@if(Setting::get('conf-color'))
    <meta name="theme-color" content="{{Setting::get('conf-color')}}">
@endif