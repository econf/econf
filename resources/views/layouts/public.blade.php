<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.common-header')

    {{ Asset::css() }}
    {{ Asset::styles() }}

    <!--[if lt IE 9]>
    {{ Asset::js('ie') }}
    <![endif]-->
</head><!--/head-->

<body>
<header id="header">
{{--    <div class="container">
        <div class="row">
            <div class="col-sm-12 overflow">
                <div class="social-icons pull-right">
                    <ul class="nav nav-pills">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                        <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="navbar navbar-inverse" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                @if(Setting::has('conf-logo'))
                    <a class="navbar-brand logo" href="{{ m_action('PublicController@index') }}">
                        <h1><img src="{{ action('FileController@get', Setting::get('conf-logo')) }}" alt=""></h1>
                    </a>
                @else
                    <a class="navbar-brand" href="{{ m_action('PublicController@index') }}">
                        <h1>{{ Setting::get('conf-short_name', Setting::get('conf-name')) }}</h1>
                    </a>
                @endif

            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    @include('partials.menu-items-public', array('items' => $PublicMenu->roots()))
                </ul>
            </div>
        </div>
    </div>
</header>
<!--/#header-->

    @yield('content')

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; E-Conf {{ date('Y') }}. {{ trans('econf.layout.all_rights_reserved') }}</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

{{ Asset::js() }}
{{ Asset::scripts('footer') }}
{{ Asset::scripts('ready') }}

@yield('scripts')

</body>
</html>
