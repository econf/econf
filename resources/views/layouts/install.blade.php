<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <title>
        E-Conf
    </title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="icon" href="{{ asset('images/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon.png') }}">

    <meta name="theme-color" content="#0087C3">

{{ Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
{{ Html::style('bower_components/components-font-awesome/css/font-awesome.min.css') }}

{{ Html::style('components/admin-lte/dist/css/AdminLTE.min.css') }}
{{ Html::style('bower_components/iCheck/skins/square/blue.css') }}

{{ Html::style('css/style.css') }}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        {!! Html::image('images/logo.png', 'E-Conf') !!}
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">

        @yield('content')

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


{{ Html::script('bower_components/jquery/dist/jquery.min.js') }}
{{ Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') }}

</body>
</html>