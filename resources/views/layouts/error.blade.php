<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $conference['name'] }}</title>
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/components-font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ $conference['stylesheet'] }}">
    <style>

        body{
            padding: 60px 0;
        }

        #error-page{
            padding: 30px 0;
        }

        /* Colors */
        .green {
            color: #5cb85c;
        }

        .orange {
            color: #f0ad4e;
        }

        .red {
            color: #d9534f;
        }

        /* Logo */
        .logo{
            padding-bottom: 1em;
        }

        .logo img{
            max-width: 100%;
            max-height: 10em;
            width: auto;
            height: auto;
        }

        .error-code{
            margin-top: 4rem;
            font-style: italic;
            font-size: .9em;
            color: #999;
            text-align: center;
        }

        .error-image span{
            font-size: 150px;
        }
    </style>
</head><!--/head-->

<body>

<div class="container">
    <div class="row">
        <div class="logo col-xs-10 col-xs-push-1 col-sm-8 col-sm-push-2 col-md-6 col-md-push-3 col-lg-4 col-lg-push-4 text-center">
            @if($conference['logo'])
                {!! Html::image($conference['logo']) !!}
            @else
                <h2>{{ $conference['name'] }}</h2>
            @endif
        </div>
    </div>
</div>

<section id="error-page">
    <div class="error-page-inner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        @yield('top')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    @yield('bottom')
</div>

<div class="container">
    <p class="error-code">
        {{ $id }}
    </p>
</div>

</body>
</html>