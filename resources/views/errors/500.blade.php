@extends('layouts.error')

@section('top')
    <div class="bg-404">
        <div class="error-image">
            <span class="glyphicon glyphicon-fire red"></span>
        </div>
    </div>
    <h2>{{ trans('errors.500.title') }}</h2>
    <p>{{ trans('errors.500.lead', ['name' => $conference['name']]) }}</p>
    <a href="javascript:document.location.reload(true);" class="btn btn-error">{{ trans('errors.actions.try_again') }}</a>

@endsection

@section('bottom')
    <div class="body-content">
        <div class="row">
            <div class="col-md-6">
                <h2>{{ trans('errors.what_happened') }}</h2>
                <p class="lead">{{ trans('errors.500.what_happened') }}</p>
            </div>
            <div class="col-md-6">
                <h2>{{ trans('errors.what_can_i_do') }}</h2>
                <p class="lead">{{ trans('errors.if_site_visitor') }}</p>
                <p>{{ trans('errors.500.site_visitor') }}</p>
                <p class="lead">{{ trans('errors.if_site_owner') }}</p>
                <p>{{ trans('errors.500.site_owner') }}</p>
            </div>
        </div>
    </div>
@endsection