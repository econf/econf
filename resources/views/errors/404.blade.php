@extends('layouts.error')

@section('top')
    <div class="bg-404">
        <div class="error-image">
            <span class="fa fa-frown-o red"></span>
        </div>
    </div>
    <h2>{{ trans('errors.404.title') }}</h2>
    <p>{{ trans('errors.404.lead', ['name' => $conference['name']]) }}</p>
    <a href="{{ $conference['home_url'] }}" class="btn btn-error">{{ trans('errors.actions.homepage') }}</a>

@endsection

@section('bottom')
    <div class="body-content">
        <div class="row">
            <div class="col-md-6">
                <h2>{{ trans('errors.what_happened') }}</h2>
                <p class="lead">{{ trans('errors.404.what_happened') }}</p>
            </div>
            <div class="col-md-6">
                <h2>{{ trans('errors.what_can_i_do') }}</h2>
                <p class="lead">{{ trans('errors.if_site_visitor') }}</p>
                <p>{{ trans('errors.404.site_visitor') }}</p>
                <p class="lead">{{ trans('errors.if_site_owner') }}</p>
                <p>{{ trans('errors.404.site_owner') }}</p>
            </div>
        </div>
    </div>
@endsection