@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-lock"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ trans('econf.user.security') }}</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <ul class="nav nav-tabs">
            <li role="presentation">
                <a href="{{ m_action('AccountController@getProfile') }}">{{ trans('econf.user.profile') }}</a>
            </li>
            <li role="presentation" class="active">
                <a href="{{ m_action('AccountController@getSecurity') }}">{{ trans('econf.user.security') }}</a>
            </li>
        </ul>

        <p></p>

        @include('flash::message')

        {!! BootForm::open()->action(m_action('AccountController@postSecurity'))->post() !!}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> {{ trans('econf.user.change_password') }}</h3>
            </div>
            <div class="panel-body">
                {!! BootForm::password(trans('econf.user.current_password'), 'current_password') !!}
                {!! BootForm::password(trans('econf.user.new_password'), 'new_password') !!}
                {!! BootForm::password(trans('econf.user.confirm_password'), 'new_password_confirmation') !!}
            </div>
            <div class="panel-footer">
                {!! BootForm::submit(trans('econf.actions.change'), 'btn-primary') !!}
            </div>
        </div>
        {!! BootForm::close() !!}

        <div class="panel @if(Auth::user()->g2fa_key) panel-success @else panel-danger @endif">
            <div class="panel-heading">
                <h3 class="panel-title">{{ trans('econf.user.2fa.title') }}</h3>
            </div>
            <div class="panel-body">
                <p>{{ trans('econf.user.2fa.description') }}</p>
                <p>{{ trans('econf.user.2fa.implementation', ['name' => 'E-Conf']) }}</p>
                @if(Auth::user()->g2fa_key)
                    <p class="text-success">{!! trans('econf.user.2fa.enabled') !!}</p>
                @else
                    <p class="text-danger">{!! trans('econf.user.2fa.disabled') !!}</p>
                @endif
            </div>
            <div class="panel-footer">
                @if(Auth::user()->g2fa_key)
                    <a class="btn btn-danger" data-toggle="modal" data-target="#twoFactorModal">
                        {{ trans('econf.user.2fa.disable') }}
                    </a>
                @else
                    <a class="btn btn-success" data-toggle="modal" data-target="#twoFactorModal">
                        {{ trans('econf.user.2fa.enable') }}
                    </a>
                @endif
            </div>
        </div>


        @if(Auth::user()->g2fa_key)
            {{-- Disable modal --}}
            <div class="modal modal-danger" id="twoFactorModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">{{ trans('econf.user.2fa.disable') }}</h4>
                        </div>
                        {!! BootForm::open()->action(m_action('AccountController@deleteTwoFactor'))->delete() !!}
                        <div class="modal-body">

                            <p>{{ trans('econf.user.2fa.disable_instructions') }}</p>
                            {!! BootForm::text(trans('econf.user.2fa.code'), 'code') !!}

                        </div>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-default pull-left"
                                    data-dismiss="modal">{{ trans('econf.actions.cancel') }}</button>
                            {!! BootForm::submit(trans('econf.user.2fa.disable'), 'btn-danger') !!}
                        </div>
                        {!! BootForm::close() !!}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @else
            <div class="modal" id="twoFactorModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">{{ trans('econf.user.2fa.enable') }}</h4>
                        </div>
                        {!! BootForm::open()->action(m_action('AccountController@postTwoFactor'))->post() !!}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-5 col-sm-push-7 text-center text-sm-right">
                                    {{ Html::image($g2fa_url) }}
                                </div>
                                <div class="col-sm-7 col-sm-pull-5">
                                    <p>{{ trans('econf.user.2fa.instructions') }}</p>
                                    {!! BootForm::text(trans('econf.user.2fa.code'), 'code') !!}
                                    {!! BootForm::hidden('token')->value($token) !!}
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer clearfix">
                            <button type="button" class="btn btn-default pull-left"
                                    data-dismiss="modal">{{ trans('econf.actions.cancel') }}</button>
                            {!! BootForm::submit(trans('econf.user.2fa.enable'), 'btn-primary') !!}
                        </div>
                        {!! BootForm::close() !!}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endif
    </div>
@endsection