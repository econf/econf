@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-user"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ trans('econf.user.profile') }}</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <ul class="nav nav-tabs">
            <li role="presentation" class="active">
                <a href="{{ m_action('AccountController@getProfile') }}">{{ trans('econf.user.profile') }}</a>
            </li>
            <li role="presentation">
                <a href="{{ m_action('AccountController@getSecurity') }}">{{ trans('econf.user.security') }}</a>
            </li>
        </ul>

        @include('flash::message')

        {!! BootForm::open()->action(m_action('AccountController@postProfile'))->post() !!}

        {!! BootForm::text(trans('econf.user.name'), 'name')->value(Auth::user()->name) !!}
        {!! BootForm::email(trans('econf.user.email'), 'email')->value(Auth::user()->email) !!}

        @action('me.profile.show', Auth::user())

        {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}

        {!! BootForm::close() !!}
    </div>

@endsection