@extends('layouts.public')

@section('content')

    @include('flash::message')

    <section id="home-slider">
        <div class="container">
            <div class="main-slider">
                <div class="slide-text">
                    <h1>{{ Setting::get('conf-name') }}</h1>
                    @if(Setting::has('conf-start_date') && Setting::has('conf-end_date'))
                        <p>
                            {{ Date::parse(Setting::get('conf-start_date'))->format(trans('econf.date.longDate')) }}
                            {{ trans('econf.public.to') }}
                            {{ Date::parse(Setting::get('conf-end_date'))->format(trans('econf.date.longDate')) }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!--/#home-slider-->

    <div class="container">
        @action('public.index')
    </div>

@endsection