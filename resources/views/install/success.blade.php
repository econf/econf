@extends('layouts.install')

@section('content')


    <h3>{{ trans('econf.installer.complete') }}</h3>

    <p>{{ trans('econf.installer.ready') }}</p>

    <p>
        {!! trans('econf.installer.instructions',
        [
        'specific' => config('econf.multi')?trans('econf.installer.instructions_multi'):trans('econf.installer.instructions_single'),
        'url' => $url
        ]) !!}
    </p>

    <p>
        {{ trans('econf.installer.thank_you') }}
    </p>

    <div class="row">
        <!-- /.col -->
        <div class="col-xs-4 col-xs-push-8">
            <a href="{{ $url }}" class="btn btn-default btn-block btn-flat">{{ trans('econf.auth.login') }}</a>
        </div>
        <!-- /.col -->
    </div>


@endsection