@extends('layouts.install')

@section('content')


    <h3>{{ trans('econf.installer.label') }}</h3>

    <p>{{ trans('econf.installer.welcome') }}</p>

    <p>
        {!! trans('econf.installer.multi_state', ['state' => config('econf.multi')?trans('econf.installer.enabled'):trans('econf.installer.disabled')]) !!}
    </p>

    <p>
        @if(config('econf.multi'))
            {{ trans('econf.installer.intro_multi') }}
        @else
            {{ trans('econf.installer.intro_single') }}
        @endif
    </p>

    <div class="row">
        <!-- /.col -->
        <div class="col-xs-4 col-xs-push-8">
            <a href="{{ action('InstallerController@form') }}?locale={{ App::getLocale() }}"
               class="btn btn-primary btn-block btn-flat">{{ trans('pagination.next') }}</a>
        </div>
        <!-- /.col -->
    </div>

    <div class="row language" style="margin-top: 20px;padding-top: 12px;margin-bottom: -8px;border-top: 1px solid #EEEEEE;color: #9E9E9E;">
        <div class="col-xs-2" style="font-size: 1.6em;margin-top: -.4rem;">
            <span class="fa fa-language"></span>
        </div>
        <div class="col-xs-10 text-right">
            @foreach(config('languages') as $code => $name)
                &nbsp;&nbsp;
                @if($code == App::getLocale())
                    <strong>{{ $name }}</strong>
                @else
                    <a href="{{ action('InstallerController@index') }}?locale={{ $code }}">{{ $name }}</a>
                @endif
            @endforeach
        </div>
    </div>




@endsection