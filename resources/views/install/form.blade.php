@extends('layouts.install')

@section('content')

    {!! BootForm::open()->action(action('InstallerController@store'))->post() !!}

    <h3>{{ trans('econf.installer.label') }}</h3>

    @unless(config('econf.multi'))
        <h4>{{ trans('econf.admin.settings.conference.data') }}</h4>

        {!! BootForm::text(trans('econf.admin.settings.conference.name'), 'conf_name') !!}
    @endunless

    <h4>{{ trans('econf.user.account') }}</h4>

    {!! BootForm::text(trans('econf.user.name'), 'name') !!}
    {!! BootForm::email(trans('econf.user.email'), 'email') !!}
    {!! BootForm::password(trans('econf.user.password'), 'password') !!}
    {!! BootForm::password(trans('econf.user.confirm_password'), 'password_confirmation') !!}

    <input type="hidden" name="locale" value="{{ App::getLocale() }}">


    <div class="row">
        <!-- /.col -->
        <div class="col-xs-4 col-xs-push-8">
            {!! BootForm::submit(trans('pagination.next'), 'btn-primary btn-block btn-flat') !!}
        </div>
        <!-- /.col -->
    </div>

    {!! BootForm::close() !!}


@endsection