@extends('layouts.email')

@section('content')

    <h2 class="first">{{ trans($trans_ns.'.subject', ['name' => $invitee_name]) }}</h2>

    <p>{{ trans($trans_ns.'.top', ['name' => $invitee_name]) }}</p>

    <ul>
        @foreach(Eventy::filter('invite.messages.'.$invite->data('type'), [], $invite) as $message)
            <li>
                <strong>{{ $message['title'] }}</strong>:
                {{ $message['text'] }}
            </li>
        @endforeach
    </ul>


@endsection