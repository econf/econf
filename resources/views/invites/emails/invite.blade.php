@extends('layouts.email')

@section('content')

    <h2 class="first">{{ trans($trans_ns.'.subject', $trans_data) }}</h2>

    {!! $msg !!}

    <hr>

    <p>{{ trans($trans_ns.'.top', $trans_data) }}</p>

    <p>{!! trans('econf.invite.bottom', ['link' => m_action('InviteController@invite', $invite->token)]) !!}</p>

    <!-- button -->
    <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <a href="{{ m_action('InviteController@invite', $invite->token) }}">{{trans('econf.actions.go')}}</a>
            </td>
        </tr>
    </table>
    <!-- /button -->


@endsection