@extends('layouts.public')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <h1>Invite</h1>

            <p>
                {!! trans('econf.invite.was_invited_to', ['email' => $invite->email]) !!}
            </p>

            <ul class="list-group">
                @foreach(Eventy::filter('invite.messages.'.$invite->data('type'), [], $invite) as $message)
                    <li class="list-group-item">
                        <h4 class="list-group-item-heading">{{ $message['title'] }}</h4>
                        <p class="list-group-item-text">{{ $message['text'] }}</p>
                    </li>
                @endforeach
            </ul>

            <div class="row">
                <div class="col-xs-6">
                    {!! BootForm::open()->action(m_action('InviteController@accept', $invite->token))->post() !!}
                    {!! BootForm::submit(trans('econf.actions.accept'), 'btn-primary btn-block') !!}
                    {!! BootForm::close() !!}
                </div>
                <div class="col-xs-6">
                    {!! BootForm::open()->action(m_action('InviteController@refuse', $invite->token))->post() !!}
                    {!! BootForm::submit(trans('econf.actions.refuse'), 'btn-danger btn-block') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection