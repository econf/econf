@foreach($items as $item)
    <li@lm-attrs($item) @if($item->hasChildren())class ="treeview"@endif @lm-endattrs>
    @if($item->link) <a@lm-attrs($item->link) @lm-endattrs href="{!! $item->url() !!}">
    {!! $item->title !!}
    @if($item->hasChildren()) <span class="fa fa-angle-left pull-right"></span> @endif
    </a>
    @else
        {!! $item->title !!}
    @endif
    @if($item->hasChildren())
        <ul class="treeview-menu">
            @include('partials.menu-items',
    array('items' => $item->children()))
        </ul>
        @endif
        </li>
        @if($item->divider)
            <li{{!! Lavary\Menu\Builder::attributes($item->divider) !!}}></li>
        @endif
        @endforeach