@if ($breadcrumbs)
    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$breadcrumb->last)
                <li>
                    <a href="{{ $breadcrumb->url }}">
                        @if($breadcrumb->first)
                            <span class="fa fa-dashboard"></span>
                        @endif
                        {{ $breadcrumb->title }}
                    </a>
                </li>
            @else
                <li class="active">
                    @if($breadcrumb->first)
                        <span class="fa fa-dashboard"></span>
                    @endif
                    {{ $breadcrumb->title }}
                </li>
            @endif
        @endforeach
    </ol>
@endif
