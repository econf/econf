@extends('layouts.public')

@section('content')

    <div class="row">
        <div class="col-md-4 col-md-push-4 col-sm-8 col-sm-push-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! BootForm::open()->action(m_action('Auth\AuthController@login'))->post() !!}
                    {!! BootForm::email(trans('econf.user.email'), 'email')->value(old('email')) !!}
                    {!! BootForm::password(trans('econf.user.password'), 'password') !!}
                    {!! BootForm::checkbox(trans('econf.auth.remember_me'), 'remember') !!}
                    <div class="text-right">
                        {!! BootForm::submit(trans('econf.auth.login'), 'btn-primary') !!}
                    </div>
                    {!! BootForm::close() !!}
                </div>
            </div>
            <p class="text-center">
                <a href="{{ m_action('Auth\PasswordController@showResetForm') }}">
                    {{trans('econf.auth.forgot_password')}}
                </a>
            </p>
            <p class="text-center">
                <a href="{{ m_action('Auth\AuthController@showRegistrationForm') }}">
                    {{trans('econf.auth.create_account')}}
                </a>
            </p>


        </div>
    </div>

@endsection