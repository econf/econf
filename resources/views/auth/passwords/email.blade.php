@extends('layouts.public')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-push-4 col-sm-8 col-sm-push-2">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        {{trans('econf.auth.reset_password')}}
                    </h3>
                </div>
                <div class="panel-body">
                    {!! BootForm::open()->action(m_action('Auth\PasswordController@sendResetLinkEmail'))->post() !!}
                    {!! BootForm::email(trans('econf.user.email'), 'email')->value(old('email')) !!}
                    {!! BootForm::submit(trans('econf.auth.send_link'), 'btn-primary') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection