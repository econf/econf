@extends('layouts.public')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-push-4 col-sm-8 col-sm-push-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        {{trans('econf.auth.reset_password')}}
                    </h3>
                </div>
                <div class="panel-body">
                    {!! BootForm::open()->action(m_action('Auth\PasswordController@reset'))->post() !!}
                    {!! BootForm::hidden('token')->value($token) !!}
                    {!! BootForm::email(trans('econf.user.email'), 'email')->value($email or old('email')) !!}
                    {!! BootForm::password(trans('econf.user.password'), 'password') !!}
                    {!! BootForm::password(trans('econf.user.confirm_password'), 'password_confirmation') !!}
                    {!! BootForm::submit(trans('econf.auth.reset'), 'btn-primary') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection