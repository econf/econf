@extends('layouts.public')

@section('content')

    <div class="row">
        <div class="col-md-4 col-md-push-4 col-sm-8 col-sm-push-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! BootForm::open()->action(m_action('Auth\AuthController@twoFactor'))->post() !!}

                    <p class="text-center">
                        <strong>{{ $user->short_name }}</strong>
                    </p>

                    {!! BootForm::text(trans('econf.user.2fa.code'), 'code') !!}

                    <div class="text-right">
                        {!! BootForm::submit(trans('econf.auth.login'), 'btn-primary') !!}
                    </div>
                    {!! BootForm::close() !!}
                </div>
            </div>


        </div>
    </div>

@endsection
