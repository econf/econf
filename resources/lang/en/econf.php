<?php

return [
	'auth' => [
		'remember_me' => 'Remember me',
		'forgot_password' => 'I forgot my password',
		'create_account' => 'Create an account',
		'login' => 'Login',
		'logout' => 'Logout',
		'reset_password' => 'Reset password',
		'reset' => 'Reset',
		'send_link' => 'Send link',
		'reset_password_mail' => [
			'click_below' => 'Click below to reset your password:',
			'subject' => 'Password reset'
		]
	],
	'network' => [
		'network' => 'Network',
		'conferences' => 'Conferences',
		'new_conference' => 'New conference',
		'new_conference_success' => 'Conference created successfully.',
		'edit_conference' => 'Edit conference',
		'edit_conference_success' => 'Conference updated successfully.',
		'delete_conference' => 'Delete conference',
		'delete_conference_confirmation' => 'Are you sure you want to delete <strong>:name</strong>?',
		'delete_conference_warning' => '<strong>Warning!</strong> This action is irreversible. Deleting a conference will delete <strong>all</strong> its data.',
		'delete_conference_success' => 'Conference deleted successfully.',
		'slug' => 'Slug',
		'name' => 'Name',
		'conference_data' => 'Conference data',
		'conference_chair_data' => 'Conference chair data',
		'visit_site' => 'Visit site',
		'conference_organizing_committee' => 'Conference organizing committee',
		'new_conf_email' => [
			'subject' => 'Your new conference is ready',
			'top' => 'You can now use E-Conf to organize :name.',
			'credentials' => 'Your credentials are:',
			'bottom' => 'You can visit your conference page at :link.'
		],
		'users' => 'Users',
		'new_user' => 'New user',
		'new_user_success' => 'User created successfully.',
		'user_data' => 'User data',
		'edit_user' => 'Edit user',
		'edit_user_success' => 'User updated successfully.',
		'delete_user' => 'Delete user',
		'delete_user_confirmation' => 'Are you sure you want to delete the user <strong>:name</strong>?',
		'delete_warning' => '<strong>Warning!</strong> This action is irreversible.',
		'delete_user_success' => 'User deleted successfully.',
		'new_user_email' => [
			'subject' => 'Your new account on E-Conf',
			'top' => 'A new account has been created for you on E-Conf.',
			'credentials' => 'Your credentials are:',
			'bottom' => 'You can visit the network admin page at :link.'
		],
		'admin' => 'Network administrator',
	],
	'user' => [
		'personal' => 'Personal',
		'account' => 'Account',
		'security' => 'Security',
		'profile' => 'Profile',
		'profile_success' => 'Your profile was updated successfully.',
		'email' => 'Email address',
		'name' => 'Name',
		'password' => 'Password',
		'confirm_password' => 'Confirm password',
		'current_password' => 'Current password',
		'new_password' => 'New password',
		'current_password_incorrect' => 'Your current password is incorrect.',
		'password_to_this_email' => 'The user\'s password will be sent to this email.',
		'change_password' => 'Change password',
		'change_password_success' => 'Password changed successfully',
		'2fa' => [
			'title' => 'Two-factor authentication',
			'description' => 'Two-factor authentication, or 2FA, is a way of logging into websites that requires more than just a password. Using a password to log into a website is susceptible to security threats, because it represents a single piece of information a malicious person needs to acquire. The added security that 2FA provides is requiring additional information to sign in.',
			'implementation' => 'You can secure your :name account using 2FA. In this case, the additional information is an authentication code generated by an application on your phone.',
			'enabled' => 'Two-factor authentication is <strong>enabled</strong> for this account.',
			'disabled' => 'Two-factor authentication is <strong>disabled</strong> for this account.',
			'enable' => 'Enable 2FA',
			'disable' => 'Disable 2FA',
			'instructions' => 'Scan this QR Code with your 2FA app. When the app is configured, type the generated code below.',
			'disable_instructions' => 'Insert your code below to disable 2FA.',
			'code' => 'Code',
			'enabled_success' => 'Two-factor authentication was enabled successfully.',
			'disabled_success' => 'Two-factor authentication was disabled successfully.',
			'wrong_code' => 'The code you entered is wrong.',
		],
	],
	'dashboard' => [
		'dashboard' => 'Dashboard',
		'conferences' => 'Conferences',
		'users' => 'Users',
	],
	'actions' => [
		'view' => 'View',
		'create' => 'Create',
		'edit' => 'Edit',
		'go' => 'Go',
		'update' => 'Update',
		'delete' => 'Delete',
		'cancel' => 'Cancel',
		'change' => 'Change',
		'send' => 'Send',
		'send_email' => 'Send email',
		'add' => 'Add',
		'remove' => 'Remove',
		'accept' => 'Accept',
		'refuse' => 'Refuse',
		'invite' => 'Invite',
		'save' => 'Save',
	],
	'layout' => [
		'all_rights_reserved' => 'All rights reserved.',
		'results' => 'Results :from to :to of :total',
		'file_formats' => 'The uploaded file must be in one of the following formats: :types',
		'delete_warning' => '<strong>Warning!</strong> This action is irreversible.',
		'phone_help' => 'The phone number must include the country code (beginning with a +).',
	],
	'admin' => [
		'view_public_page' => 'View public page',
		'label' => 'Administration',
		'conference' => 'Conference',
		'important_dates' => 'Important dates',
		'settings' => [
			'label' => 'Settings',
			'conference' => [
				'label' => 'Conference settings',
				'label_short' => 'Conference',
				'success' => 'Settings saved successfully.',
				'data' => 'Conference data',
				'name' => 'Name',
				'short_name' => 'Short name',
				'email' => 'Email address',
				'start_date' => 'Start date',
				'end_date' => 'End date',
				'topics' => 'Topics',
				'topics_one-per-line' => 'Write each topic on a separate line.',
				'brand' => 'Conference brand',
				'logo' => 'Logo',
				'logo_white' => 'White logo',
				'icon' => 'Icon',
				'icon_help' => 'This icon will be used as a favicon and for mobile devices. The image must be a square.',
				'color' => 'Color',
				'language' => 'Language',
			]
		],
		'organization' => 'Organization',
		'organizing_committee' => 'Organizing Commitee',
		'email' => [
			'label' => 'Email',
			'pick_recipient' => 'Pick recipient',
			'group' => 'Group',
			'select_individual' => 'Select individual recipients',
			'template' => 'Template',
			'all_users' => 'All users',
			'blank_template' => 'Blank template',
			'recipients' => 'Recipients',
			'compose' => 'Compose',
			'message' => 'Message',
			'subject' => 'Subject',
			'variables' => 'Variables',
			'message_details' => 'Message details',
			'to' => 'To:',
			'vars' => [
				'confName' => 'Conference name',
				'confShortName' => 'Conference short name',
				'name' => 'Recipient name',
				'email' => 'Recipient email',
			],
		],
	],
	'session_types' => [
		'label' => 'Session types',
		'name' => 'Name',
		'duration' => 'Duration',
		'duration_min' => 'Duration (min.)',
		'parallel' => 'Parallel',
		'minutes' => 'minutes',
		'minutes_each' => 'minutes each',
		'create_success' => 'Session type created successfully.',
		'update_success' => 'Session type updated successfully.',
		'delete_success' => 'Session type deleted successfully.',
		'create' => 'Create new session type',
		'edit' => 'Edit session type',
		'delete' => 'Delete session type',
		'delete_confirmation' => 'Are you sure you want to delete the session type <strong>:name</strong>?',
	],
	'public' => [
		'dashboard' => [
			'label' => 'Dashboard',
			'me' => 'Me',
		],
		'to' => 'to',
	],
	'date' => [
		'medium' => 'M j, Y g:i A',
		'short' => 'm/d/y g:i A',
		'fullDate' => 'l, F j, Y',
		'longDate' => 'F j, Y',
		'mediumDate' => 'M j, Y',
		'shortDate' => 'm/d/y',
		'dayMonth' => 'M j',
		'time' => 'g:i A'
	],
	'committee' => [
		'chair' => 'Chair',
		'make_chair' => 'Make chair',
		'remove_chair' => 'Remove chair',
		'invite_user' => 'Invite user to committee',
		'remove_user' => 'Remove user from committee',
		'remove_confirmation' => 'Are you sure you want to remove <strong>:name</strong> from the :committee?',
		'remove_user_success' => ':name was successfully removed from the committee.',
		'make_chair_success' => ':name was successfully made chair of the committee.',
		'remove_chair_success' => ':name was successfully removed as chair of the committee.',
		'invite_email' => [
			'subject' => 'You were invited to be a part of the :committee for :conf_name',
			'top' => 'You were invited to be a part of the :committee for :conf_name.',
		],
		'invite_text' => 'Be a member of :committee',
		'invite_text_chair' => 'Be a chair of :committee',
		'is_part' => ':user is already part of this committee.',
		'invite_accepted' => 'You are now a member of the :committee.',
		'invite_sent' => 'The invites were sent.',
		'one_per_line' => 'Write the e-mail addresses you want to invite, each in a separate line.',
		'user_not_part' => 'The user :name is not part of this committee.',
	],
	'invite' => [
		'bottom' => 'You can see the invite and accept it or refuse it at <a href=":link">:link</a>.',
		'was_invited_to' => '<strong>:email</strong> was invited to:',
		'login_required' => 'You have to be logged in to accept this invite.',
		'refused_success' => 'You refused the invite.',
		'accepted_email' => [
			'subject' => ':name accepted your invite',
			'top' => ':name has accepted your invite to:',
		],
		'refused_email' => [
			'subject' => ':name refused your invite',
			'top' => ':name has refused your invite to:',
		],
	],
	'email' => [
		'dear_name' => 'Dear :name,',
	],
	'misc' => [
		'all' => 'All',
		'yes' => 'Yes',
		'no' => 'No',
	],
	'data_tables' => [ // For DataTables plugin
		'processing' => 'Processing…',
		'search' => 'Search:',
		'lengthMenu' => 'Show _MENU_ results',
		'info' => 'Results _START_ to _END_ of _TOTAL_', // Should be identical to $[layout][results]
		'infoEmpty' => 'No results available',
		'infoFiltered' => '(filtered from _MAX_ total results)',
		'loadingRecords' => 'Loading…',
		'zeroRecords' => 'No results were found',
		'emptyTable' => 'No results available',
		'paginate' => [
			'first' => 'First',
			'previous' => 'Previous',
			'next' => 'Next',
			'last' => 'Last',
		],

	],
	'installer' => [
		'label' => 'Installer',
		'complete' => 'Installation complete',
		'welcome' => 'Welcome to the E-Conf installer.',
		'multi_state' => 'Right now, you have multi-conference <strong>:state</strong>. If this is not what you pretend, please change the <code>ECONF_MULTI</code> value in the <code>.env</code> file.',
		'enabled' => 'enabled',
		'disabled' => 'disabled',
		'intro_single' => 'This installer will help you configure your conference and create your organizer account.',
		'intro_multi' => 'This installer will help you create your network administrator account.',
		'ready' => 'Your E-Conf installation is now ready to use!',
		'instructions' => 'You can now access <a href=":url">:url</a> to login into your account and :specific.',
		'instructions_multi' => 'create your first conference',
		'instructions_single' => 'configure your conference',
		'thank_you' => 'Thank you for using E-Conf!',
	]
];