<?php
return [

	'what_happened' => 'What happened?',
	'what_can_i_do' => 'What can I do?',
	'if_site_visitor' => 'If you\'re a site visitor',
	'if_site_owner' => 'If you\'re the site owner',
	'actions' => [
		'homepage' => 'Take Me To The Homepage',
		'try_again' => 'Try This Page Again',
	],
	'403' => [
		'title' => '403 Forbidden',
		'lead' => 'Sorry! You don\'t have access permissions for that on :name.',
		'what_happened' => 'A 403 error status indicates that you don\'t have permission to access the file or page. In general, web servers and websites have directories and files that are not open to the public web for security reasons.',
		'site_visitor' => 'Please use your browser\'s back button and check that you\'re in the right place. If you need immediate assistance, please send us an email instead.',
		'site_owner' => 'Please check that you\'re in the right place and get in touch with your website provider if you believe this to be an error. The error ID below may be useful.',
	],
	'404' => [
		'title' => '404 Not Found',
		'lead' => 'We couldn\'t find what you\'re looking for on :name.',
		'what_happened' => 'A 404 error status implies that the file or page that you\'re looking for could not be found.',
		'site_visitor' => 'Please use your browser\'s back button and check that you\'re in the right place. If you need immediate assistance, please send us an email instead.',
		'site_owner' => 'Please check that you\'re in the right place and get in touch with your website provider if you believe this to be an error. The error ID below may be useful.',
	],
	'500' => [
		'title' => '500 Internal Server Error',
		'lead' => 'The web server is returning an internal error for :name.',
		'what_happened' => 'A 500 error status implies there is a problem with the web server\'s software causing it to malfunction.',
		'site_visitor' => 'Nothing you can do at the moment. If you need immediate assistance, please send us an email instead. We apologize for any inconvenience.',
		'site_owner' => 'This error can only be fixed by server admins, please contact your website provider. The error ID below may be useful.',
	],
	'503' => [
		'title' => 'Temporary Maintenance',
		'lead' => 'The web server for :name is currently undergoing some maintenance.',
		'what_happened' => 'Servers and websites need regular maintenance just like a car to keep them up and running smoothly.',
		'site_visitor' => 'We will most likely be back very shortly. If you need immediate assistance, please send us an email instead. We apologize for any inconvenience.',
		'site_owner' => 'The maintenance period will mostly likely be very brief, the best thing to do is to check back in a few minutes and everything will probably be working normal again.',
	],

];