<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A password deverá conter pelo menos seis carateres e ser igual à confirmação.',
    'reset' => 'A sua password foi redefinida!',
    'sent' => 'A ligação para recuperar a sua password foi enviada para o seu e-mail!',
    'token' => 'Este código de recuperação da password é inválido.',
    'user' => "Não existe nenhum utilizador com o endereço de e-mail indicado.",

];
