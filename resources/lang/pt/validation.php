<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O campo :attribute deverá ser aceite.',
    'active_url'           => 'O campo :attribute não contém um URL válido.',
    'after'                => 'O campo :attribute deverá conter uma data posterior a :date.',
    'alpha'                => 'O campo :attribute deverá conter apenas letras.',
    'alpha_dash'           => 'O campo :attribute deverá conter apenas letras, números e traços.',
    'alpha_num'            => 'O campo :attribute deverá conter apenas letras e números .',
    'array'                => 'O campo :attribute deverá conter uma coleção de elementos.',
    'before'               => 'O campo :attribute deverá conter uma data anterior a :date.',
    'between'              => [
        'numeric' => 'O campo :attribute deverá ter um valor entre :min e :max.',
        'file'    => 'O campo :attribute deverá ter um tamanho entre :min e :max kilobytes.',
        'string'  => 'O campo :attribute deverá conter entre :min e :max caracteres.',
        'array'   => 'O campo :attribute deverá conter entre :min e :max elementos.',
    ],
    'boolean'              => 'O campo :attribute deverá ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação para o campo :attribute não coincide.',
    'date'                 => 'O campo :attribute não contém uma data válida.',
    'date_format'          => 'A data indicada para o campo :attribute não respeita o formato :format.',
    'different'            => 'Os campos :attribute e :other deverão conter valores diferentes.',
    'digits'               => 'O campo :attribute deverá conter :digits caracteres.',
    'digits_between'       => 'O campo :attribute deverá conter entre :min e :max caracteres.',
    'email'                => 'O campo :attribute não contém um endereço de e-mail válido.',
    'exists'               => 'O valor selecionado para o campo :attribute é inválido.',
    'filled'               => 'O campo :attribute é obrigatório.',
    'image'                => 'O campo :attribute deverá ser uma imagem.',
    'in'                   => 'O campo :attribute não contém um valor válido.',
    'integer'              => 'O campo :attribute deverá conter um número inteiro.',
    'ip'                   => 'O campo :attribute deverá conter um IP válido.',
    'json'                 => 'O campo :attribute deverá conter um texto JSON válido.',
    'max'                  => [
        'numeric' => 'O campo :attribute não deverá conter um valor superior a :max.',
        'file'    => 'O campo :attribute não deverá ter um tamanho superior a :max kilobytes.',
        'string'  => 'O campo :attribute não deverá conter mais de :max caracteres.',
        'array'   => 'O campo :attribute não deverá conter mais de :max elementos.',
    ],
    'mimes'                => 'O campo :attribute deverá conter um ficheiro do tipo: :values.',
    'min'                  => [
        'numeric' => 'O campo :attribute deverá ter um valor superior ou igual a :min.',
        'file'    => 'O campo :attribute deverá ter no mínimo :min kilobytes.',
        'string'  => 'O campo :attribute deverá conter no mínimo :min caracteres.',
        'array'   => 'O campo :attribute deverá conter no mínimo :min elementos.',
    ],
    'not_in'               => 'O campo :attribute contém um valor inválido.',
    'numeric'              => 'O campo :attribute deverá conter um valor numérico.',
    'regex'                => 'O formato do valor para o campo :attribute é inválido.',
    'required'             => 'O campo :attribute é obrigatório.',
    'required_if'          => 'O campo :attribute é obrigatório quando o valor do campo :other é igual a :value.',
    'required_unless'      => 'O campo :attribute é obrigatório a menos que :other esteja presente em :values.',
    'required_with'        => 'O campo :attribute é obrigatório quando :values está presente.',
    'required_with_all'    => 'O campo :attribute é obrigatório quando um dos :values está presente.',
    'required_without'     => 'O campo :attribute é obrigatório quando :values não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum dos :values está presente.',
    'same'                 => 'Os campos :attribute e :other deverão conter valores iguais.',
    'size'                 => [
        'numeric' => 'O campo :attribute deverá conter o valor :size.',
        'file'    => 'O campo :attribute deverá ter o tamanho de :size kilobytes.',
        'string'  => 'O campo :attribute deverá conter :size caracteres.',
        'array'   => 'O campo :attribute deverá conter :size elementos.',
    ],
    'string'               => 'O campo :attribute deverá conter texto.',
    'timezone'             => 'O campo :attribute deverá ter um fuso horário válido.',
    'unique'               => 'O valor indicado para o campo :attribute já se encontra registado.',
    'url'                  => 'O formato do URL indicado para o campo :attribute é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'country' => 'O campo :attribute deverá conter um país válido.',
    'words' => 'O campo :attribute não deve exceder :count palavras.',
    'phone' => 'O campo :attribute deverá conter um número de telefone válido.',

    /*'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],*/

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'setting.conf-name' => 'nome',
        'setting.conf-short_name' => 'nome curto',
        'setting.conf-start_date' => 'data de início',
        'setting.conf-end_date' => 'data de fim',
        'logo' => 'logótipo',
        'logo_white' => 'logótipo a branco',
        'setting.conf-color' => 'cor',
    ],

];
