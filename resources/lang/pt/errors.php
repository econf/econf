<?php
return [

	'what_happened' => 'O que aconteceu?',
	'what_can_i_do' => 'O que posso fazer?',
	'if_site_visitor' => 'Se é um visitante do site',
	'if_site_owner' => 'Se é o dono do site',
	'actions' => [
		'homepage' => 'Leve-me para a Página Inicial',
		'try_again' => 'Tentar esta Página Novamente',
	],
	'403' => [
		'title' => '403 Proibido',
		'lead' => 'Desculpe! Não tem permissões para isso em :name.',
		'what_happened' => 'Um código de erro 403 indica que não tem permissão para aceder a este ficheiro ou página. No geral, os servidores e site têm pastas e ficheiros que não estão abertos ao público por motivos de segurança.',
		'site_visitor' => 'Por favor utilize o botão de Retroceder do seu navegador e confirme que está no sítio certo. Se precisa assistência imediata, opte por nos enviar um e-mail.',
		'site_owner' => 'Por favor confirme se está no sítio certo e entre em contacto com o provedor do seu site se acredita que isto é um erro. O ID de erro abaixo poderá ser útil.',
	],
	'404' => [
		'title' => '404 Não Encontrado',
		'lead' => 'Não conseguimos encontrar o que procura em :name.',
		'what_happened' => 'Um código de erro 404 implica que o ficheiro ou página que procura não foi encontrado.',
		'site_visitor' => 'Por favor utilize o botão de Retroceder do seu navegador e confirme que está no sítio certo. Se precisa assistência imediata, opte por nos enviar um e-mail.',
		'site_owner' => 'Por favor confirme se está no sítio certo e entre em contacto com o provedor do seu site se acredita que isto é um erro. O ID de erro abaixo poderá ser útil.',
	],
	'500' => [
		'title' => '500 Erro Interno do Servidor',
		'lead' => 'O servidor está a devolver um erro interno para :name.',
		'what_happened' => 'Um código de erro 500 implica que há um problema no software do servidor a fazer com que funcione mal.',
		'site_visitor' => 'Não há nada que possa fazer agora. Se precisa assistência imediata, opte por nos enviar um e-mail. Pedimos desculpa pelo incómodo.',
		'site_owner' => 'Este erro só pode ser resolvido por administradores do sistema, por favor contacte o provedor do seu site. O ID de erro abaixo poderá ser útil.',
	],
	'503' => [
		'title' => 'Manutenção Temporária',
		'lead' => 'O servidor para :name está a passar por uma manutenção.',
		'what_happened' => 'Servidores e sites precisam de manutenção regular, como um carro para o manter a funcionar na perfeição.',
		'site_visitor' => 'Muito provavelmente voltaremos muito em breve. Se precisa assistência imediata, opte por nos enviar um e-mail. Pedimos desculpa pelo incómodo.',
		'site_owner' => 'O período de manutenção será provavelmente muito breve, o melhor a fazer é verificar novamente daqui a uns minutos, quando tudo deverá estar a funcionar normalmente de novo.',
	],

];