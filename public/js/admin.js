$(document).ready(function(){
    $('.colorinput').colorpicker();

    $('textarea').autogrow({
        onInitialize: true
    });

    // KOALA CUSTOM <hr>
    Koala.addCommand("horizontalRule", function (editor, value) {
        document.execCommand("insertHorizontalRule", false, value);
    }, function (editor, value) {
        return document.queryCommandState("insertHorizontalRule");
    });
    Koala.addButton({name: "hr", label: "Horizontal Rule", icon: "fa-minus", command: "horizontalRule"});

    $('.koala-box.simple textarea, .koala-simple').koala({
        buttons: ['bold', 'italic', 'underline', 'sep', 'ul', 'ol', 'sep', 'undo', 'redo'],
        language: locale
    });

    $('.koala-box.medium textarea, .koala-medium').koala({
        buttons: ['bold', 'italic', 'underline', 'sep', 'ul', 'ol', 'hr', 'sep', 'undo', 'redo', 'sep', 'code'],
        language: locale
    });

    $('.select2').select2();

    $(document).on('submit', 'form', function(e) {
        var $form = $(this),
            $button;
        $form.find(':submit').each(function() {
            $button = $(this);
            $button.prop('disabled', true).width($button.width()).html('<span class="fa fa-circle-o-notch fa-spin"></span>');
        });
        return true;
    });

    $('[data-toggle="popover"]').popover({
        container: 'body',
        placement: 'auto bottom',
    });

});