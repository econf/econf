$(document).ready(function () {
    $('textarea').autogrow({
        onInitialize: true
    });

    $('.select2').select2({
        theme: 'bootstrap'
    });

    $(document).on('submit', 'form', function(e) {
        var $form = $(this),
            $button;
        $form.find(':submit').each(function() {
            $button = $(this);
            $button.prop('disabled', true).width($button.width()).html('<span class="fa fa-circle-o-notch fa-spin"></span>');
        });
    });

    //Responsive Nav
    $('.navbar-nav li.dropdown > a').each(function(){
        $(this).on('click', function(){
            if( $(window).width() < 768 ) {
                $(this).next().slideToggle();
            }
            return false;
        });
    });
});