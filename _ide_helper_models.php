<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Committee
 *
 * @property integer $id
 * @property string $name
 * @property integer $conference_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $trans_key
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Committee whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Committee whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Committee whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Committee whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Committee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Committee whereTransKey($value)
 */
	class Committee extends \Eloquent {}
}

namespace App{
/**
 * App\Conference
 *
 * @property integer $id
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Conference whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Conference whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Conference whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Conference whereUpdatedAt($value)
 */
	class Conference extends \Eloquent {}
}

namespace App{
/**
 * App\Invite
 *
 * @property integer $id
 * @property string $email
 * @property string $token
 * @property string $data
 * @property integer $conference_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Invite whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invite whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invite whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invite whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invite whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invite whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Invite whereUpdatedAt($value)
 */
	class Invite extends \Eloquent {}
}

namespace App{
/**
 * App\SessionType
 *
 * @property integer $id
 * @property string $name
 * @property integer $duration
 * @property integer $conference_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $parallel
 * @method static \Illuminate\Database\Query\Builder|\App\SessionType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SessionType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SessionType whereDuration($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SessionType whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SessionType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SessionType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\SessionType whereParallel($value)
 */
	class SessionType extends \Eloquent {}
}

namespace App{
/**
 * App\Topic
 *
 * @property integer $id
 * @property string $name
 * @property integer $conference_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Topic whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Topic whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Topic whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Topic whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Topic whereUpdatedAt($value)
 */
	class Topic extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $g2fa_key
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $conference_id
 * @property string $data
 * @property-read mixed $photo
 * @property-read mixed $short_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Committee[] $committees
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereG2faKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereData($value)
 */
	class User extends \Eloquent {}
}

namespace EConf\Submissions{
/**
 * EConf\Submissions\Submission
 *
 * @property integer $id
 * @property string $title
 * @property integer $session_type_id
 * @property string $data
 * @property integer $conference_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $user_id
 * @property boolean $accepted
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Topic[] $topics
 * @property-read \App\User $user
 * @property-read \App\SessionType $session_type
 * @property-read mixed $hidden_id
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereSessionTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Submissions\Submission whereAccepted($value)
 */
	class Submission extends \Eloquent {}
}

namespace EConf\Reviews{
/**
 * EConf\Reviews\Bid
 *
 * @property integer $id
 * @property integer $submission_id
 * @property integer $user_id
 * @property integer $conference_id
 * @property string $bid
 * @property boolean $locked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \EConf\Submissions\Submission $submission
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Bid whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Bid whereSubmissionId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Bid whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Bid whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Bid whereBid($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Bid whereLocked($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Bid whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Bid whereUpdatedAt($value)
 */
	class Bid extends \Eloquent {}
}

namespace EConf\Reviews{
/**
 * EConf\Reviews\Review
 *
 * @property integer $id
 * @property integer $submission_id
 * @property integer $user_id
 * @property integer $assignee_id
 * @property integer $conference_id
 * @property string $data
 * @property float $score
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \EConf\Submissions\Submission $submission
 * @property-read \App\User $assignee
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereSubmissionId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereAssigneeId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereScore($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\Reviews\Review whereUpdatedAt($value)
 */
	class Review extends \Eloquent {}
}

namespace EConf\ProgramManagement{
/**
 * EConf\ProgramManagement\Event
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon $end_time
 * @property boolean $secondary
 * @property integer $room_id
 * @property integer $session_id
 * @property integer $conference_id
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \EConf\ProgramManagement\Session $session
 * @property-read \EConf\ProgramManagement\Room $room
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereStartTime($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereEndTime($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereSecondary($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereRoomId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereSessionId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Event findSimilarSlugs($model, $attribute, $config, $slug)
 */
	class Event extends \Eloquent {}
}

namespace EConf\ProgramManagement{
/**
 * EConf\ProgramManagement\Room
 *
 * @property integer $id
 * @property string $name
 * @property integer $capacity
 * @property string $slug
 * @property integer $conference_id
 * @property string $data
 * @property integer $venue_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \EConf\ProgramManagement\Venue $venue
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereCapacity($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereVenueId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Room findSimilarSlugs($model, $attribute, $config, $slug)
 */
	class Room extends \Eloquent {}
}

namespace EConf\ProgramManagement{
/**
 * EConf\ProgramManagement\Session
 *
 * @property integer $id
 * @property integer $session_type_id
 * @property integer $topic_id
 * @property integer $conference_id
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Topic $topic
 * @property-read \App\SessionType $session_type
 * @property-read \EConf\ProgramManagement\Event $event
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Session whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Session whereSessionTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Session whereTopicId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Session whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Session whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Session whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Session whereUpdatedAt($value)
 */
	class Session extends \Eloquent {}
}

namespace EConf\ProgramManagement{
/**
 * EConf\ProgramManagement\Venue
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $address
 * @property float $latitude
 * @property float $longitude
 * @property integer $conference_id
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\EConf\ProgramManagement\Room[] $rooms
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereConferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\EConf\ProgramManagement\Venue findSimilarSlugs($model, $attribute, $config, $slug)
 */
	class Venue extends \Eloquent {}
}

